
package persistencia;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ArchivoLectura {

    private String linea = "";
    private BufferedReader in;

    public ArchivoLectura(String unNombre) {
        try {
            in = new BufferedReader(new FileReader(unNombre));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ArchivoLectura.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean hayMasLineas() {
        boolean hayMas;
        try {
            linea = in.readLine();
        } catch (IOException e) {
        }
        hayMas = linea != null;
        return hayMas;
    }
    
    public String[] retornarJugadores(){
        String[] alias = new String[2];
        
        try {
            
            alias[0] = in.readLine();
            
            alias[1] = in.readLine();
        } catch (IOException ex) {
            
        }
        
        return alias;
    }

    public String linea() {
        return linea;
    }

    public boolean cerrar() {
        boolean ok = true;
        try {
            in.close();
        } catch (Exception e) {
            ok = false;
        }
        return ok;
    }

}
