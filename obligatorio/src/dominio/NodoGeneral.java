package dominio;

import java.util.ArrayList;

/**
 * Creado por hadexexplade el 01 de febrero del 2016
 */
public class NodoGeneral<E> implements Elemento<E> {

    private E elemento;
    private int nodoId;
    private NodoGeneral<E> padre;
    private E pareja;
    private ArrayList<NodoGeneral<E>> hijos;

    public NodoGeneral() {

    }

    public NodoGeneral(E elemento, NodoGeneral<E> padre, int id) {
        this.elemento = elemento;
        this.padre = padre;
        this.hijos = new ArrayList<NodoGeneral<E>>();
        this.nodoId = id;
    }

    @Override
    public E getElemento() {
        return elemento;
    }

    public void setElemento(E elemento) {
        this.elemento = elemento;
    }

    public NodoGeneral<E> getPadre() {
        return padre;
    }

    public void setPadre(NodoGeneral<E> padre) {
        this.padre = padre;
    }

    public void setPareja(E info) {
        this.pareja = info;
    }

    public E getPareja() {
        return pareja;
    }

    public void insertarHijo(NodoGeneral<E> elemento) {
        hijos.add(elemento);

    }

    public int cantHijos() {
        return hijos.size();
    }

    public ArrayList<Elemento<E>> hijos() {
        return new ArrayList<Elemento<E>>(hijos);
    }

    public int getNodoId() {
        return nodoId;
    }

}
