package dominio;

/**
 * Creado por hadexexplade el 01 de febrero del 2016
 * @param <E>
 */
public interface Elemento<E> {

    E getElemento();
}
