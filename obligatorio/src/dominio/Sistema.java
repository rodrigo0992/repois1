package dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;

public class Sistema extends Observable implements Serializable {

    private ArbolGeneral<Persona> arbolGeneral;
    private ArrayList<Recuerdo> recuerdos;

    public Sistema() {
        this.arbolGeneral = new ArbolGeneral<Persona>();
        this.recuerdos = new ArrayList<Recuerdo>();
    }

    public ArbolGeneral<Persona> getArbolGeneral() {
        return arbolGeneral;
    }

    public ArrayList<Recuerdo> getRecuerdos() {
        return recuerdos;
    }

    public void agregarRecuerdo(String imagen, String descripcion, ArrayList<Persona> unaPersona) {

        recuerdos.add(new Recuerdo(imagen, descripcion, unaPersona));
    }

    public Recuerdo getRecuerdo(String descripcion) {
        Recuerdo resultado = null;
        for (Recuerdo recuerdo : recuerdos) {
            if (descripcion.compareTo(recuerdo.getDescripcion()) == 0) {
                resultado = recuerdo;
                break;
            }
        }
        return resultado;
    }

    public void agregarPrimeraPersonaArbol(String unNombre, String unApellido, String unLugar,
            String unaFechaNacimiento, String unaNacionalidad, String unaImagen,
            int unSexo, int unEstadoCivil, int unEstadoSalud) {

        Persona p = new Persona(unNombre, unApellido, unLugar, unaFechaNacimiento,
                unaNacionalidad, unaImagen, unSexo, unEstadoCivil, unEstadoSalud);

        arbolGeneral.insertarRaiz(p);
    }

    public Persona crearPersonaNodoArbol(String unNombre, String unApellido, String unLugar,
            String unaFechaNacimiento, String unaNacionalidad, String unaImagen,
            int unSexo, int unEstadoCivil, int unEstadoSalud) {

        Persona p = new Persona(unNombre, unApellido, unLugar, unaFechaNacimiento,
                unaNacionalidad, unaImagen, unSexo, unEstadoCivil, unEstadoSalud);

        return p;
    }

}
