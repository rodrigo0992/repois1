/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;

public class Recuerdo {

    private String imagen;
    private String descripcion;
    private ArrayList<Persona> personas;

    public Recuerdo(String imagen, String descripcion, ArrayList<Persona> personas) {
        this.imagen = imagen;
        this.descripcion = descripcion;
        this.personas = personas;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<Persona> getPersonas() {
        return personas;
    }
}
