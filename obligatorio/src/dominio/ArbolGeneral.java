package dominio;

import java.util.ArrayList;

/**
 * Creado por hadexexplade el 01 de febrero del 2016
 * @param <E>
 */
public class ArbolGeneral<E> {

    private NodoGeneral<E> raiz;
    private int cantidad;

    public ArbolGeneral() {
        this.cantidad = 0;
    }

    public NodoGeneral<E> getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoGeneral<E> raiz) {
        this.raiz = raiz;
    }

    public int getCantidad() {
        return cantidad;
    }

    private NodoGeneral<E> crearNodo(E e, NodoGeneral<E> padre, int id) {
        return new NodoGeneral<E>(e, padre, id);
    }

    private NodoGeneral<E> validar(Elemento<E> elemento) {
        if (!(elemento instanceof NodoGeneral)) {
            return null;
        }
        NodoGeneral<E> nodoGeneral = (NodoGeneral<E>) elemento;
        return nodoGeneral.getPadre() == nodoGeneral ? null : nodoGeneral;
    }

    public boolean esRaiz(Elemento<E> elemento) {
        return elemento == getRaiz();
    }

    public boolean estaVacia() {
        return getCantidad() == 0;
    }

    public Elemento<E> obtenerPadre(Elemento<E> elemento) {
        return validar(elemento).getPadre();
    }

    public int getNodoId(Elemento<E> elemento) {
        return validar(elemento).getNodoId();
    }

    public void getElementoPorNodoId(int nodoId, Elemento<E> raiz, NodoGeneral<Elemento<E>> nodoGeneral) {
        if (nodoId == validar(raiz).getNodoId()) {
            nodoGeneral.setElemento(raiz);
        }

        for (Elemento<E> x : obtenerHijos(raiz)) {
            getElementoPorNodoId(nodoId, x, nodoGeneral);
        }
    }

    public int cantidadHijos(Elemento<E> elemento) {
        NodoGeneral<E> nodoGeneral = validar(elemento);
        return nodoGeneral.cantHijos();
    }

    public ArrayList<Elemento<E>> obtenerHijos(Elemento<E> elemento) {
        NodoGeneral<E> nodoGeneral = validar(elemento);
        return nodoGeneral.hijos();
    }

    public Elemento<E> insertarRaiz(E info) {
        if (!estaVacia()) {
            return null;
        }
        raiz = crearNodo(info, null, 1);
        cantidad = 1;
        return raiz;
    }

    public Elemento<E> insertarHijo(Elemento<E> elemento, E info) {
        NodoGeneral<E> nodoGeneral = validar(elemento);

        if (nodoGeneral == null) {
            return null;
        }
        cantidad++;
        NodoGeneral<E> hijo = crearNodo(info, nodoGeneral, cantidad);
        nodoGeneral.insertarHijo(hijo);
        return hijo;
    }

    public Elemento<E> insertarPadreRaiz(Elemento<E> elemento, E info) {
        NodoGeneral<E> nodoGeneral = validar(elemento);

        if (esRaiz(elemento) && nodoGeneral != null) {
            cantidad++;
            NodoGeneral<E> padre = crearNodo(info, null, cantidad);
            nodoGeneral.setPadre(padre);
            padre.insertarHijo(nodoGeneral);
            setRaiz(padre);
            return padre;
        }
        return null;
    }

    public E setPareja(Elemento<E> elemento, E info) {
        NodoGeneral<E> nodoGeneral = validar(elemento);
        if (nodoGeneral != null) {
            nodoGeneral.setPareja(info);
            return info;
        }
        return null;
    }

    public boolean tienePareja(Elemento<E> elemento) {
        return validar(elemento).getPareja() != null;
    }

    public void borrarPersona(Persona unaPersona) {
        unaPersona.setPersonaVacia(unaPersona);
    }

    public ArrayList<Elemento<E>> recorridoPreOrden() {
        ArrayList<Elemento<E>> pos = new ArrayList<Elemento<E>>();
        if (!estaVacia()) {
            recorridoPreOrden(getRaiz(), pos);
        }
        return pos;
    }

    private void recorridoPreOrden(Elemento<E> raiz, ArrayList<Elemento<E>> pos) {
        pos.add(raiz);
        for (Elemento<E> hijo : obtenerHijos(raiz)) {
            recorridoPreOrden(hijo, pos);
        }
    }

    public E obtenerPareja(Elemento<E> elemento) {

        return validar(elemento).getPareja();
    }

}
