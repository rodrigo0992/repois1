package dominio;

public class Persona {

    private String nombre;
    private String apellido;
    private String lugar;
    private String fechaNacimiento;
    private String nacionalidad;
    private String imagen;
    private int sexo;
    private int estadoCivil;
    private int estadoSalud;

    public Persona(String unNombre, String unApellido, String unLugar,
            String unaFechaNacimiento, String unaNacionalidad, String unaImagen,
            int unSexo, int unEstadoCivil, int unEstadoSalud) {
        this.setNombre(unNombre);
        this.setApellido(unApellido);
        this.setLugar(unLugar);
        this.setFechaNacimiento(unaFechaNacimiento);
        this.setNacionalidad(unaNacionalidad);
        this.setImagen(unaImagen);
        this.setSexo(unSexo);
        this.setEstadiCivil(unEstadoCivil);
        this.setEstadoSalud(unEstadoSalud);
    }

    public Persona() {
        this.setNombre("");
        this.setApellido("");
        this.setLugar("");
        this.setFechaNacimiento("");
        this.setNacionalidad("");
        this.setImagen("/Imagenes/addPerson3.png");
        this.setSexo(0);
        this.setEstadiCivil(0);
        this.setEstadoSalud(1);
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getLugar() {
        return lugar;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public int getSexo() {
        return sexo;
    }

    public int getEstadoCivil() {
        return estadoCivil;
    }

    public int getEstadoSalud() {
        return estadoSalud;
    }

    public String getImagen() {
        return imagen;
    }

    public void setNombre(String unNombre) {
        this.nombre = unNombre;
    }

    public void setApellido(String unApellido) {
        this.apellido = unApellido;
    }

    public void setLugar(String unLugar) {
        this.lugar = unLugar;
    }

    public void setFechaNacimiento(String unaFechaNacimiento) {
        this.fechaNacimiento = unaFechaNacimiento;
    }

    public void setNacionalidad(String unaNacionalidad) {
        this.nacionalidad = unaNacionalidad;
    }

    public void setSexo(int unSexo) {
        this.sexo = unSexo;
    }

    public void setEstadiCivil(int unEstadoCivil) {
        this.estadoCivil = unEstadoCivil;
    }

    public void setEstadoSalud(int unEstadoSalud) {
        this.estadoSalud = unEstadoSalud;
    }

    public void setImagen(String unaImagen) {
        this.imagen = unaImagen;
    }

    public void setPersonaVacia(Persona unaPersona) {
        unaPersona.setNombre("");
        unaPersona.setApellido("");
        unaPersona.setLugar("");
        unaPersona.setFechaNacimiento("");
        unaPersona.setNacionalidad("");
        unaPersona.setImagen("/Imagenes/addPerson3.png");
        unaPersona.setSexo(0);
        unaPersona.setEstadiCivil(0);
        unaPersona.setEstadoSalud(1);
    }

    @Override
    public String toString() {
        return this.getNombre() + " " + this.getApellido();
    }
}
