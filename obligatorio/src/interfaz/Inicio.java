package interfaz;

import dominio.Sistema;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Inicio {

    public static void main(String[] args) {

        Sistema sistema = new Sistema();

        try {
            FileInputStream fff = new FileInputStream("archivo");
            BufferedInputStream bbb = new BufferedInputStream(fff);
            ObjectInputStream sss = new ObjectInputStream(bbb);

            sistema = (Sistema) (sss.readObject());

        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {
            Logger.getLogger(Inicio.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Inicio.class.getName()).log(Level.SEVERE, null, ex);
        }

        VentanaMenuPrincipal v = new VentanaMenuPrincipal(sistema);
        v.setVisible(true);

    }
}
