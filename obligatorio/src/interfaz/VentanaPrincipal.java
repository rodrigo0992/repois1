package interfaz;

import dominio.ArbolGeneral;
import dominio.Elemento;
import dominio.NodoGeneral;
import dominio.Persona;
import dominio.Sistema;
import java.awt.Color;
import java.awt.Image;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.filechooser.FileNameExtensionFilter;

public class VentanaPrincipal extends javax.swing.JFrame {

    private Sistema objSistema;
    private FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivo de imagenes", "jpg");
    private ImageIcon imagenPersona;
    private ImageIcon imagenPersonaSinResize;
    private String urlImagen;
    private Elemento<Persona> elementoPersona;
    private int tipoPersona;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public VentanaPrincipal(Sistema modelo) {
        objSistema = modelo;
        initComponents();

        tipoPersona = 0;
        elementoPersona = null;
        ponerFotoPorDefecto();
        buttomGroupSexo.add(radioButton1Sexo);
        buttomGroupSexo.add(radioButton2Sexo);
        buttonGroupEstadoDeSalud.add(radioButtom1EstadoDeSalud);
        buttonGroupEstadoDeSalud.add(radioButtom2EstadoDeSalud);
        botonBorrarPareja.setVisible(false);
        labelBorrarPareja.setVisible(false);
        vaciarFormulario();
        mostrarArbol();
        labelTxtSeleccionado.setVisible(false);
        jLabelSeleccionado.setVisible(false);
    }

    public FileNameExtensionFilter getFilter() {
        return filter;
    }

    public void setFilter(FileNameExtensionFilter filter) {
        this.filter = filter;
    }

    public ImageIcon getImagenPersona() {
        return imagenPersona;
    }

    public void setImagenPersona(ImageIcon unaImgPersona) {
        this.imagenPersona = unaImgPersona;
    }

    public ImageIcon getImagenPersonaSinResize() {
        return imagenPersonaSinResize;
    }

    public void setImagenPersonaSinResize(ImageIcon unaImgPersona) {
        this.imagenPersonaSinResize = unaImgPersona;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttomGroupSexo = new javax.swing.ButtonGroup();
        buttonGroupEstadoDeSalud = new javax.swing.ButtonGroup();
        labelTxtAgregarPadre = new javax.swing.JLabel();
        labelTxtAgregarPareja = new javax.swing.JLabel();
        labelTxtAgregarHijo = new javax.swing.JLabel();
        labelTxtBuscar = new javax.swing.JLabel();
        labelTxtBuscarError = new javax.swing.JLabel();
        labelTxtVerArbol = new javax.swing.JLabel();
        labelTxtRecuerdos = new javax.swing.JLabel();
        labelBorrarPareja = new javax.swing.JLabel();
        labelTxtEliminarPersona = new javax.swing.JLabel();
        labelTxtAyuda = new javax.swing.JLabel();
        textFieldBuscar = new javax.swing.JTextField();
        botonBuscar = new javax.swing.JButton();
        botonAyuda = new javax.swing.JToggleButton();
        botonAgregarPadre = new javax.swing.JButton();
        botonAgregarPareja = new javax.swing.JToggleButton();
        botonAgregarHijo = new javax.swing.JToggleButton();
        botonVerArbol = new javax.swing.JButton();
        botonRecuerdos = new javax.swing.JToggleButton();
        botonBorrarPersona = new javax.swing.JToggleButton();
        botonBorrarPareja = new javax.swing.JToggleButton();
        jLabelSeleccionado = new javax.swing.JLabel();
        labelTxtSeleccionado = new javax.swing.JLabel();
        fondoVerde = new javax.swing.JLabel();
        panelPersona = new javax.swing.JPanel();
        tituloCambiante = new javax.swing.JLabel();
        botonFotoPersona = new javax.swing.JButton();
        botonConfirmar = new javax.swing.JButton();
        dateCalendario = new com.toedter.calendar.JDateChooser();
        labelFotoPersona = new javax.swing.JLabel();
        fondoBlancoImagen = new javax.swing.JLabel();
        labeTxtlNombre = new javax.swing.JLabel();
        labelTxtErrorNombre = new javax.swing.JLabel();
        labelTxtErrorApellido = new javax.swing.JLabel();
        labelTxtApellido = new javax.swing.JLabel();
        labelTxtLugar = new javax.swing.JLabel();
        labelTxtFechaNacimiento = new javax.swing.JLabel();
        labelTxtErrorNacionalidad = new javax.swing.JLabel();
        labelTxtNacionalidad = new javax.swing.JLabel();
        labelTxtEstadoCivil = new javax.swing.JLabel();
        labelTxtSexo = new javax.swing.JLabel();
        labelTxtEstadoSalud = new javax.swing.JLabel();
        labelTxtBtnConfirmar = new javax.swing.JLabel();
        textFieldApellido = new javax.swing.JTextField();
        textFieldNombre = new javax.swing.JTextField();
        textFieldLugar = new javax.swing.JTextField();
        textFieldNacionalidad = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        comboBoxEstadoCivil = new javax.swing.JComboBox<>();
        radioButton2Sexo = new javax.swing.JRadioButton();
        radioButton1Sexo = new javax.swing.JRadioButton();
        radioButtom2EstadoDeSalud = new javax.swing.JRadioButton();
        radioButtom1EstadoDeSalud = new javax.swing.JRadioButton();
        fondoBlanco = new javax.swing.JLabel();
        panelArbol = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(1200, 700));
        setPreferredSize(new java.awt.Dimension(1200, 700));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        labelTxtAgregarPadre.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtAgregarPadre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTxtAgregarPadre.setText("Agregar Padre");
        getContentPane().add(labelTxtAgregarPadre);
        labelTxtAgregarPadre.setBounds(170, 560, 100, 16);

        labelTxtAgregarPareja.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtAgregarPareja.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTxtAgregarPareja.setText("Agregar Pareja");
        getContentPane().add(labelTxtAgregarPareja);
        labelTxtAgregarPareja.setBounds(280, 560, 90, 16);

        labelTxtAgregarHijo.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtAgregarHijo.setText("Agregar Hijo");
        getContentPane().add(labelTxtAgregarHijo);
        labelTxtAgregarHijo.setBounds(390, 560, 80, 16);

        labelTxtBuscar.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtBuscar.setText("Buscar Persona");
        getContentPane().add(labelTxtBuscar);
        labelTxtBuscar.setBounds(40, 560, 90, 16);

        labelTxtBuscarError.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        getContentPane().add(labelTxtBuscarError);
        labelTxtBuscarError.setBounds(40, 540, 170, 20);

        labelTxtVerArbol.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtVerArbol.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTxtVerArbol.setText("Ver Arbol");
        getContentPane().add(labelTxtVerArbol);
        labelTxtVerArbol.setBounds(630, 560, 90, 16);

        labelTxtRecuerdos.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtRecuerdos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTxtRecuerdos.setText("Recuerdos");
        getContentPane().add(labelTxtRecuerdos);
        labelTxtRecuerdos.setBounds(710, 560, 90, 16);

        labelBorrarPareja.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelBorrarPareja.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelBorrarPareja.setText("Eliminar Pareja");
        getContentPane().add(labelBorrarPareja);
        labelBorrarPareja.setBounds(500, 610, 100, 20);

        labelTxtEliminarPersona.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtEliminarPersona.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTxtEliminarPersona.setText("Eliminar Persona");
        getContentPane().add(labelTxtEliminarPersona);
        labelTxtEliminarPersona.setBounds(500, 560, 100, 16);

        labelTxtAyuda.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtAyuda.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTxtAyuda.setText("Ayuda");
        getContentPane().add(labelTxtAyuda);
        labelTxtAyuda.setBounds(680, 610, 60, 20);

        textFieldBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textFieldBuscarKeyTyped(evt);
            }
        });
        getContentPane().add(textFieldBuscar);
        textFieldBuscar.setBounds(40, 580, 80, 30);

        botonBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/2.png"))); // NOI18N
        botonBuscar.setBorderPainted(false);
        botonBuscar.setContentAreaFilled(false);
        botonBuscar.setFocusPainted(false);
        botonBuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                botonBuscarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                botonBuscarMouseExited(evt);
            }
        });
        botonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarActionPerformed(evt);
            }
        });
        getContentPane().add(botonBuscar);
        botonBuscar.setBounds(120, 570, 50, 50);

        botonAyuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/help2.png"))); // NOI18N
        botonAyuda.setBorderPainted(false);
        botonAyuda.setContentAreaFilled(false);
        botonAyuda.setFocusPainted(false);
        botonAyuda.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                botonAyudaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                botonAyudaMouseExited(evt);
            }
        });
        botonAyuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAyudaActionPerformed(evt);
            }
        });
        getContentPane().add(botonAyuda);
        botonAyuda.setBounds(690, 630, 40, 40);

        botonAgregarPadre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/addPadre2.png"))); // NOI18N
        botonAgregarPadre.setBorderPainted(false);
        botonAgregarPadre.setContentAreaFilled(false);
        botonAgregarPadre.setFocusPainted(false);
        botonAgregarPadre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                botonAgregarPadreMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                botonAgregarPadreMouseExited(evt);
            }
        });
        botonAgregarPadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarPadreActionPerformed(evt);
            }
        });
        getContentPane().add(botonAgregarPadre);
        botonAgregarPadre.setBounds(190, 570, 50, 50);

        botonAgregarPareja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/addPareja2.png"))); // NOI18N
        botonAgregarPareja.setBorderPainted(false);
        botonAgregarPareja.setContentAreaFilled(false);
        botonAgregarPareja.setFocusPainted(false);
        botonAgregarPareja.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                botonAgregarParejaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                botonAgregarParejaMouseExited(evt);
            }
        });
        botonAgregarPareja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarParejaActionPerformed(evt);
            }
        });
        getContentPane().add(botonAgregarPareja);
        botonAgregarPareja.setBounds(300, 570, 50, 50);

        botonAgregarHijo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/addHijo2.png"))); // NOI18N
        botonAgregarHijo.setBorderPainted(false);
        botonAgregarHijo.setContentAreaFilled(false);
        botonAgregarHijo.setFocusPainted(false);
        botonAgregarHijo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                botonAgregarHijoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                botonAgregarHijoMouseExited(evt);
            }
        });
        botonAgregarHijo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarHijoActionPerformed(evt);
            }
        });
        getContentPane().add(botonAgregarHijo);
        botonAgregarHijo.setBounds(400, 570, 50, 50);

        botonVerArbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/list2.png"))); // NOI18N
        botonVerArbol.setBorderPainted(false);
        botonVerArbol.setContentAreaFilled(false);
        botonVerArbol.setFocusPainted(false);
        botonVerArbol.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                botonVerArbolMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                botonVerArbolMouseExited(evt);
            }
        });
        botonVerArbol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonVerArbolActionPerformed(evt);
            }
        });
        getContentPane().add(botonVerArbol);
        botonVerArbol.setBounds(660, 570, 40, 40);

        botonRecuerdos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/gallery22.png"))); // NOI18N
        botonRecuerdos.setBorderPainted(false);
        botonRecuerdos.setContentAreaFilled(false);
        botonRecuerdos.setFocusPainted(false);
        botonRecuerdos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                botonRecuerdosMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                botonRecuerdosMouseExited(evt);
            }
        });
        botonRecuerdos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonRecuerdosActionPerformed(evt);
            }
        });
        getContentPane().add(botonRecuerdos);
        botonRecuerdos.setBounds(730, 570, 40, 40);

        botonBorrarPersona.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/borrar2.png"))); // NOI18N
        botonBorrarPersona.setBorderPainted(false);
        botonBorrarPersona.setContentAreaFilled(false);
        botonBorrarPersona.setFocusPainted(false);
        botonBorrarPersona.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                botonBorrarPersonaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                botonBorrarPersonaMouseExited(evt);
            }
        });
        botonBorrarPersona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBorrarPersonaActionPerformed(evt);
            }
        });
        getContentPane().add(botonBorrarPersona);
        botonBorrarPersona.setBounds(520, 570, 50, 50);

        botonBorrarPareja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/borrar2.png"))); // NOI18N
        botonBorrarPareja.setBorderPainted(false);
        botonBorrarPareja.setContentAreaFilled(false);
        botonBorrarPareja.setFocusPainted(false);
        botonBorrarPareja.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                botonBorrarParejaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                botonBorrarParejaMouseExited(evt);
            }
        });
        botonBorrarPareja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBorrarParejaActionPerformed(evt);
            }
        });
        getContentPane().add(botonBorrarPareja);
        botonBorrarPareja.setBounds(520, 630, 50, 40);

        jLabelSeleccionado.setBackground(new java.awt.Color(0, 153, 204));
        jLabelSeleccionado.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jLabelSeleccionado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelSeleccionado.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51)));
        jLabelSeleccionado.setOpaque(true);
        getContentPane().add(jLabelSeleccionado);
        jLabelSeleccionado.setBounds(140, 630, 50, 20);

        labelTxtSeleccionado.setBackground(new java.awt.Color(0, 153, 204));
        labelTxtSeleccionado.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtSeleccionado.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtSeleccionado.setText("Persona Actual:");
        labelTxtSeleccionado.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51)));
        labelTxtSeleccionado.setOpaque(true);
        getContentPane().add(labelTxtSeleccionado);
        labelTxtSeleccionado.setBounds(40, 630, 150, 20);

        fondoVerde.setBackground(new java.awt.Color(152, 222, 142));
        fondoVerde.setForeground(new java.awt.Color(51, 51, 51));
        fondoVerde.setOpaque(true);
        getContentPane().add(fondoVerde);
        fondoVerde.setBounds(0, 530, 800, 160);

        panelPersona.setBackground(new java.awt.Color(0, 153, 204));
        panelPersona.setMinimumSize(new java.awt.Dimension(400, 700));
        panelPersona.setLayout(null);

        tituloCambiante.setFont(new java.awt.Font("Century Gothic", 2, 40)); // NOI18N
        tituloCambiante.setForeground(new java.awt.Color(51, 51, 51));
        tituloCambiante.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelPersona.add(tituloCambiante);
        tituloCambiante.setBounds(0, 0, 400, 60);

        botonFotoPersona.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/add.png"))); // NOI18N
        botonFotoPersona.setBorderPainted(false);
        botonFotoPersona.setContentAreaFilled(false);
        botonFotoPersona.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        botonFotoPersona.setFocusPainted(false);
        botonFotoPersona.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                botonFotoPersonaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                botonFotoPersonaMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                botonFotoPersonaMousePressed(evt);
            }
        });
        botonFotoPersona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonFotoPersonaActionPerformed(evt);
            }
        });
        panelPersona.add(botonFotoPersona);
        botonFotoPersona.setBounds(230, 150, 40, 40);

        botonConfirmar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/tick3.png"))); // NOI18N
        botonConfirmar.setBorderPainted(false);
        botonConfirmar.setContentAreaFilled(false);
        botonConfirmar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        botonConfirmar.setFocusable(false);
        botonConfirmar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                botonConfirmarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                botonConfirmarMouseExited(evt);
            }
        });
        botonConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonConfirmarActionPerformed(evt);
            }
        });
        panelPersona.add(botonConfirmar);
        botonConfirmar.setBounds(310, 620, 40, 40);

        dateCalendario.setDateFormatString("dd/MM/yyyy");
        panelPersona.add(dateCalendario);
        dateCalendario.setBounds(30, 390, 120, 20);

        labelFotoPersona.setBackground(new java.awt.Color(255, 255, 255));
        labelFotoPersona.setOpaque(true);
        panelPersona.add(labelFotoPersona);
        labelFotoPersona.setBounds(150, 70, 100, 100);

        fondoBlancoImagen.setBackground(new java.awt.Color(255, 255, 255));
        fondoBlancoImagen.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        fondoBlancoImagen.setOpaque(true);
        panelPersona.add(fondoBlancoImagen);
        fondoBlancoImagen.setBounds(130, 60, 140, 130);

        labeTxtlNombre.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labeTxtlNombre.setForeground(new java.awt.Color(51, 51, 51));
        labeTxtlNombre.setText("Nombre");
        panelPersona.add(labeTxtlNombre);
        labeTxtlNombre.setBounds(30, 190, 50, 20);

        labelTxtErrorNombre.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        panelPersona.add(labelTxtErrorNombre);
        labelTxtErrorNombre.setBounds(90, 190, 180, 20);

        labelTxtErrorApellido.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        panelPersona.add(labelTxtErrorApellido);
        labelTxtErrorApellido.setBounds(90, 240, 180, 30);

        labelTxtApellido.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtApellido.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtApellido.setText("Apellido");
        panelPersona.add(labelTxtApellido);
        labelTxtApellido.setBounds(30, 250, 50, 14);

        labelTxtLugar.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtLugar.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtLugar.setText("Lugar");
        panelPersona.add(labelTxtLugar);
        labelTxtLugar.setBounds(30, 310, 40, 14);

        labelTxtFechaNacimiento.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtFechaNacimiento.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtFechaNacimiento.setText("Fecha de Nacimiento");
        panelPersona.add(labelTxtFechaNacimiento);
        labelTxtFechaNacimiento.setBounds(30, 370, 140, 14);

        labelTxtErrorNacionalidad.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        panelPersona.add(labelTxtErrorNacionalidad);
        labelTxtErrorNacionalidad.setBounds(120, 420, 180, 30);

        labelTxtNacionalidad.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtNacionalidad.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtNacionalidad.setText("Nacionalidad");
        panelPersona.add(labelTxtNacionalidad);
        labelTxtNacionalidad.setBounds(30, 430, 90, 14);

        labelTxtEstadoCivil.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtEstadoCivil.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtEstadoCivil.setText("Estado Civil");
        panelPersona.add(labelTxtEstadoCivil);
        labelTxtEstadoCivil.setBounds(30, 490, 70, 14);

        labelTxtSexo.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtSexo.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtSexo.setText("Sexo");
        panelPersona.add(labelTxtSexo);
        labelTxtSexo.setBounds(30, 550, 30, 14);

        labelTxtEstadoSalud.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtEstadoSalud.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtEstadoSalud.setText("Estado de Salud");
        panelPersona.add(labelTxtEstadoSalud);
        labelTxtEstadoSalud.setBounds(30, 610, 100, 14);

        labelTxtBtnConfirmar.setBackground(new java.awt.Color(255, 255, 255));
        labelTxtBtnConfirmar.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        labelTxtBtnConfirmar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTxtBtnConfirmar.setOpaque(true);
        panelPersona.add(labelTxtBtnConfirmar);
        labelTxtBtnConfirmar.setBounds(280, 610, 90, 20);

        textFieldApellido.setBackground(new java.awt.Color(102, 255, 102));
        textFieldApellido.setBorder(null);
        textFieldApellido.setOpaque(false);
        textFieldApellido.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                textFieldApellidoMouseClicked(evt);
            }
        });
        textFieldApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textFieldApellidoKeyTyped(evt);
            }
        });
        panelPersona.add(textFieldApellido);
        textFieldApellido.setBounds(30, 270, 330, 30);

        textFieldNombre.setBorder(null);
        textFieldNombre.setOpaque(false);
        textFieldNombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                textFieldNombreMouseClicked(evt);
            }
        });
        textFieldNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldNombreActionPerformed(evt);
            }
        });
        textFieldNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textFieldNombreKeyTyped(evt);
            }
        });
        panelPersona.add(textFieldNombre);
        textFieldNombre.setBounds(30, 210, 330, 30);

        textFieldLugar.setBorder(null);
        textFieldLugar.setOpaque(false);
        textFieldLugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldLugarActionPerformed(evt);
            }
        });
        panelPersona.add(textFieldLugar);
        textFieldLugar.setBounds(30, 330, 330, 30);

        textFieldNacionalidad.setBorder(null);
        textFieldNacionalidad.setOpaque(false);
        textFieldNacionalidad.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                textFieldNacionalidadMouseClicked(evt);
            }
        });
        textFieldNacionalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textFieldNacionalidadKeyTyped(evt);
            }
        });
        panelPersona.add(textFieldNacionalidad);
        textFieldNacionalidad.setBounds(30, 450, 330, 30);
        panelPersona.add(jSeparator3);
        jSeparator3.setBounds(30, 300, 330, 10);
        panelPersona.add(jSeparator1);
        jSeparator1.setBounds(30, 240, 330, 10);
        panelPersona.add(jSeparator5);
        jSeparator5.setBounds(30, 480, 330, 10);
        panelPersona.add(jSeparator7);
        jSeparator7.setBounds(30, 420, 330, 10);
        panelPersona.add(jSeparator6);
        jSeparator6.setBounds(30, 600, 330, 10);
        panelPersona.add(jSeparator8);
        jSeparator8.setBounds(30, 540, 330, 10);
        panelPersona.add(jSeparator4);
        jSeparator4.setBounds(30, 360, 330, 10);

        comboBoxEstadoCivil.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        comboBoxEstadoCivil.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Sin Determinar", "Soltero", "Casado", "Divorciado", "Viudo", "En Pareja" }));
        comboBoxEstadoCivil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxEstadoCivilActionPerformed(evt);
            }
        });
        panelPersona.add(comboBoxEstadoCivil);
        comboBoxEstadoCivil.setBounds(30, 510, 120, 22);

        radioButton2Sexo.setBackground(new java.awt.Color(255, 255, 255));
        radioButton2Sexo.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        radioButton2Sexo.setText("F");
        radioButton2Sexo.setFocusPainted(false);
        radioButton2Sexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButton2SexoActionPerformed(evt);
            }
        });
        panelPersona.add(radioButton2Sexo);
        radioButton2Sexo.setBounds(90, 570, 40, 20);

        radioButton1Sexo.setBackground(new java.awt.Color(255, 255, 255));
        radioButton1Sexo.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        radioButton1Sexo.setText("M");
        radioButton1Sexo.setFocusPainted(false);
        radioButton1Sexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButton1SexoActionPerformed(evt);
            }
        });
        panelPersona.add(radioButton1Sexo);
        radioButton1Sexo.setBounds(30, 570, 40, 20);

        radioButtom2EstadoDeSalud.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        radioButtom2EstadoDeSalud.setText("Difunto");
        radioButtom2EstadoDeSalud.setOpaque(false);
        panelPersona.add(radioButtom2EstadoDeSalud);
        radioButtom2EstadoDeSalud.setBounds(90, 630, 93, 25);

        radioButtom1EstadoDeSalud.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        radioButtom1EstadoDeSalud.setText("Vivo");
        radioButtom1EstadoDeSalud.setOpaque(false);
        panelPersona.add(radioButtom1EstadoDeSalud);
        radioButtom1EstadoDeSalud.setBounds(30, 630, 60, 25);

        fondoBlanco.setBackground(new java.awt.Color(255, 255, 255));
        fondoBlanco.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        fondoBlanco.setOpaque(true);
        panelPersona.add(fondoBlanco);
        fondoBlanco.setBounds(10, 130, 380, 530);

        getContentPane().add(panelPersona);
        panelPersona.setBounds(800, 0, 400, 700);

        panelArbol.setBackground(new java.awt.Color(255, 255, 255));
        panelArbol.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        panelArbol.setForeground(new java.awt.Color(255, 255, 255));
        panelArbol.setLayout(null);
        getContentPane().add(panelArbol);
        panelArbol.setBounds(0, 0, 800, 540);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void botonFotoPersonaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonFotoPersonaMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/add2.png"));
        botonFotoPersona.setIcon(img);
    }//GEN-LAST:event_botonFotoPersonaMouseEntered

    private void botonFotoPersonaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonFotoPersonaMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/add.png"));
        botonFotoPersona.setIcon(img);
    }//GEN-LAST:event_botonFotoPersonaMouseExited

    private void botonFotoPersonaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonFotoPersonaMousePressed
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/add.png"));
        botonFotoPersona.setIcon(img);
    }//GEN-LAST:event_botonFotoPersonaMousePressed

    private void botonFotoPersonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonFotoPersonaActionPerformed
        JFileChooser archivoSeleccionado = new JFileChooser();

        archivoSeleccionado.setFileFilter(this.getFilter());
        int opcion = archivoSeleccionado.showOpenDialog(null);
        if (opcion == JFileChooser.APPROVE_OPTION) {

            //url de la imagen
            String file = archivoSeleccionado.getSelectedFile().getPath();
            urlImagen = file;
            this.setImagenPersonaSinResize(new ImageIcon(file));
            this.setImagenPersona(new ImageIcon(this.getImagenPersonaSinResize().getImage().getScaledInstance(labelFotoPersona.getWidth(), labelFotoPersona.getHeight(), Image.SCALE_DEFAULT)));
            labelFotoPersona.setIcon(this.getImagenPersona());
        }
    }//GEN-LAST:event_botonFotoPersonaActionPerformed

    private void botonConfirmarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonConfirmarMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/tick.png"));
        botonConfirmar.setIcon(img);
    }//GEN-LAST:event_botonConfirmarMouseEntered

    private void botonConfirmarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonConfirmarMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/tick3.png"));
        botonConfirmar.setIcon(img);
    }//GEN-LAST:event_botonConfirmarMouseExited

    private void botonConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonConfirmarActionPerformed

        if (tipoPersona == 0) {
            JOptionPane.showMessageDialog(null, "Realize una busqueda de persona");
        } else {
            if (tipoPersona == 4) {
                EditarPersonaEnNodoArbol(elementoPersona);
            } else if (tipoPersona == 1 || tipoPersona == 3) {
                AgregarPersonaANodoArbol(elementoPersona);

            } else if (tipoPersona == 2) {
                if (objSistema.getArbolGeneral().tienePareja(elementoPersona)) {

                    Persona pareja = objSistema.getArbolGeneral().obtenerPareja(elementoPersona);
                    NodoGeneral<Persona> nodo = new NodoGeneral<Persona>();
                    nodo.setElemento(pareja);
                    EditarPersonaEnNodoArbol(nodo);

                } else {
                    AgregarPersonaANodoArbol(elementoPersona);
                }
            }
            tipoPersona = 4;
            cargarDatosDePersona(elementoPersona);
            mostrarArbol();
        }


    }//GEN-LAST:event_botonConfirmarActionPerformed

    private void textFieldApellidoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textFieldApellidoMouseClicked
        labelTxtErrorApellido.setText("");
        labelTxtErrorNombre.setText("");
        labelTxtErrorNacionalidad.setText("");
    }//GEN-LAST:event_textFieldApellidoMouseClicked

    private void textFieldApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldApellidoKeyTyped
        textoError(evt, labelTxtErrorApellido);
    }//GEN-LAST:event_textFieldApellidoKeyTyped

    private void textFieldNombreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textFieldNombreMouseClicked
        labelTxtErrorNombre.setText("");
        labelTxtErrorApellido.setText("");
        labelTxtErrorNacionalidad.setText("");
    }//GEN-LAST:event_textFieldNombreMouseClicked

    private void textFieldNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldNombreActionPerformed

    private void textFieldNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldNombreKeyTyped
        textoError(evt, labelTxtErrorNombre);
    }//GEN-LAST:event_textFieldNombreKeyTyped

    private void textFieldLugarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldLugarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldLugarActionPerformed

    private void textFieldNacionalidadMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textFieldNacionalidadMouseClicked
        labelTxtErrorNacionalidad.setText("");
        labelTxtErrorApellido.setText("");
        labelTxtErrorNombre.setText("");
    }//GEN-LAST:event_textFieldNacionalidadMouseClicked

    private void textFieldNacionalidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldNacionalidadKeyTyped
        textoError(evt, labelTxtErrorNacionalidad);
    }//GEN-LAST:event_textFieldNacionalidadKeyTyped

    private void comboBoxEstadoCivilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxEstadoCivilActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboBoxEstadoCivilActionPerformed

    private void radioButton2SexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButton2SexoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radioButton2SexoActionPerformed

    private void radioButton1SexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButton1SexoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radioButton1SexoActionPerformed

    private void botonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarActionPerformed
        String texto = textFieldBuscar.getText();
        if (!texto.equalsIgnoreCase("")) {
            int cantidad = objSistema.getArbolGeneral().getCantidad();
            int numeroABuscar = Integer.parseInt(textFieldBuscar.getText());
            if (numeroABuscar >= 1 && numeroABuscar <= cantidad) {
                //busco a la persona en el arbol
                NodoGeneral<Elemento<Persona>> nodoGeneral = new NodoGeneral<Elemento<Persona>>();
                ArbolGeneral arbol = objSistema.getArbolGeneral();
                arbol.getElementoPorNodoId(numeroABuscar, arbol.getRaiz(), nodoGeneral);
                elementoPersona = nodoGeneral.getElemento();
                jLabelSeleccionado.setText(textFieldBuscar.getText());
                labelTxtSeleccionado.setVisible(true);
                jLabelSeleccionado.setVisible(true);
                //cargo los datos de la persona
                tipoPersona = 4;
                cargarDatosDePersona(elementoPersona);

                if (!objSistema.getArbolGeneral().tienePareja(elementoPersona)) {
                    botonBorrarPareja.setVisible(false);
                    labelBorrarPareja.setVisible(false);
                } else {
                    botonBorrarPareja.setVisible(true);
                    labelBorrarPareja.setVisible(true);
                }

            } else {
                JOptionPane.showMessageDialog(null, "Elija el numero de alguna persona que aparece en el arbol");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debes seleccionar un numero");
        }


    }//GEN-LAST:event_botonBuscarActionPerformed

    private void botonAgregarPadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarPadreActionPerformed
        if (elementoPersona != null) {
            if (objSistema.getArbolGeneral().esRaiz(elementoPersona)) {

                vaciarFormulario();
                tituloCambiante.setText("Agregar Padre");
                labelTxtBtnConfirmar.setText("Agregar");
                tipoPersona = 1;
            } else {
                JOptionPane.showMessageDialog(null, "La persona ya tiene un padre creado");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debes buscar a una persona");
        }


    }//GEN-LAST:event_botonAgregarPadreActionPerformed

    private void botonAgregarParejaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarParejaActionPerformed
        if (elementoPersona != null) {
            tipoPersona = 2;
            vaciarFormulario();
            if (objSistema.getArbolGeneral().tienePareja(elementoPersona)) {

                tituloCambiante.setText("Editar Pareja");
                labelTxtBtnConfirmar.setText("confirmar");
                Persona pareja = objSistema.getArbolGeneral().obtenerPareja(elementoPersona);
                NodoGeneral<Persona> nodo = new NodoGeneral<Persona>();
                nodo.setElemento(pareja);
                cargarDatosDePersona(nodo);
            } else {

                tituloCambiante.setText("Agregar Pareja");
                labelTxtBtnConfirmar.setText("Agregar");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debes buscar a una persona");
        }
    }//GEN-LAST:event_botonAgregarParejaActionPerformed

    private void botonAgregarHijoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarHijoActionPerformed

        if (elementoPersona != null) {
            vaciarFormulario();
            tituloCambiante.setText("Agregar Hijo");
            labelTxtBtnConfirmar.setText("Agregar");
            tipoPersona = 3;

        } else {
            JOptionPane.showMessageDialog(null, "Debes buscar a una persona");
        }


    }//GEN-LAST:event_botonAgregarHijoActionPerformed

    private void textFieldBuscarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldBuscarKeyTyped
        char c = evt.getKeyChar();
        if (c > 31 && (c < 48 || c > 57)) {
            getToolkit().beep();
            labelTxtBuscarError.setText("Ingrese solamente numeros");
            labelTxtBuscarError.setForeground(Color.red);
            evt.consume();
        } else {
            labelTxtBuscarError.setText("");
        }
    }//GEN-LAST:event_textFieldBuscarKeyTyped

    private void botonAgregarHijoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonAgregarHijoMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/addHijo1.png"));
        botonAgregarHijo.setIcon(img);
    }//GEN-LAST:event_botonAgregarHijoMouseEntered

    private void botonAgregarHijoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonAgregarHijoMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/addHijo2.png"));
        botonAgregarHijo.setIcon(img);
    }//GEN-LAST:event_botonAgregarHijoMouseExited

    private void botonAgregarParejaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonAgregarParejaMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/addPareja1.png"));
        botonAgregarPareja.setIcon(img);
    }//GEN-LAST:event_botonAgregarParejaMouseEntered

    private void botonAgregarParejaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonAgregarParejaMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/addPareja2.png"));
        botonAgregarPareja.setIcon(img);
    }//GEN-LAST:event_botonAgregarParejaMouseExited

    private void botonAgregarPadreMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonAgregarPadreMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/addPadre1.png"));
        botonAgregarPadre.setIcon(img);
    }//GEN-LAST:event_botonAgregarPadreMouseEntered

    private void botonAgregarPadreMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonAgregarPadreMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/addPadre2.png"));
        botonAgregarPadre.setIcon(img);
    }//GEN-LAST:event_botonAgregarPadreMouseExited

    private void botonBuscarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonBuscarMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/1.png"));
        botonBuscar.setIcon(img);
    }//GEN-LAST:event_botonBuscarMouseEntered

    private void botonBuscarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonBuscarMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/2.png"));
        botonBuscar.setIcon(img);
    }//GEN-LAST:event_botonBuscarMouseExited

    private void botonVerArbolMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonVerArbolMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/list1.png"));
        botonVerArbol.setIcon(img);
    }//GEN-LAST:event_botonVerArbolMouseEntered

    private void botonVerArbolMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonVerArbolMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/list2.png"));
        botonVerArbol.setIcon(img);
    }//GEN-LAST:event_botonVerArbolMouseExited

    private void botonRecuerdosMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonRecuerdosMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/gallery11.png"));
        botonRecuerdos.setIcon(img);
    }//GEN-LAST:event_botonRecuerdosMouseEntered

    private void botonRecuerdosMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonRecuerdosMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/gallery22.png"));
        botonRecuerdos.setIcon(img);
    }//GEN-LAST:event_botonRecuerdosMouseExited

    private void botonVerArbolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonVerArbolActionPerformed
        ArbolEnLista arbolLista = new ArbolEnLista(objSistema);
        arbolLista.setVisible(true);
    }//GEN-LAST:event_botonVerArbolActionPerformed

    private void botonRecuerdosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonRecuerdosActionPerformed
        Recuerdos recuerdos = new Recuerdos(objSistema);
        recuerdos.setVisible(true);
    }//GEN-LAST:event_botonRecuerdosActionPerformed

    private void botonBorrarPersonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBorrarPersonaActionPerformed
        if (elementoPersona != null) {
            objSistema.getArbolGeneral().borrarPersona(elementoPersona.getElemento());
            JOptionPane.showMessageDialog(null, "Persona eliminada correctamente");
            mostrarArbol();
        } else {
            JOptionPane.showMessageDialog(null, "Debes seleccionar una persona");
        }

    }//GEN-LAST:event_botonBorrarPersonaActionPerformed

    private void botonBorrarPersonaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonBorrarPersonaMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/borrar1.png"));
        botonBorrarPersona.setIcon(img);
    }//GEN-LAST:event_botonBorrarPersonaMouseEntered

    private void botonBorrarPersonaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonBorrarPersonaMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/borrar2.png"));
        botonBorrarPersona.setIcon(img);
    }//GEN-LAST:event_botonBorrarPersonaMouseExited

    private void botonBorrarParejaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonBorrarParejaMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/borrar1.png"));
        botonBorrarPareja.setIcon(img);
    }//GEN-LAST:event_botonBorrarParejaMouseEntered

    private void botonBorrarParejaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonBorrarParejaMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/borrar2.png"));
        botonBorrarPareja.setIcon(img);
    }//GEN-LAST:event_botonBorrarParejaMouseExited

    private void botonBorrarParejaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBorrarParejaActionPerformed

        if (elementoPersona != null) {
            Persona pareja = objSistema.getArbolGeneral().obtenerPareja(elementoPersona);
            objSistema.getArbolGeneral().borrarPersona(pareja);
            JOptionPane.showMessageDialog(null, "Pareja eliminada correctamente");
            mostrarArbol();

        } else {
            JOptionPane.showMessageDialog(null, "Debes seleccionar una persona");
        }


    }//GEN-LAST:event_botonBorrarParejaActionPerformed

    private void botonAyudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAyudaActionPerformed
        VentanaAyuda v = new VentanaAyuda();
        v.setVisible(true);
    }//GEN-LAST:event_botonAyudaActionPerformed

    private void botonAyudaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonAyudaMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/help1.png"));
        botonAyuda.setIcon(img);
    }//GEN-LAST:event_botonAyudaMouseEntered

    private void botonAyudaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonAyudaMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/help2.png"));
        botonAyuda.setIcon(img);
    }//GEN-LAST:event_botonAyudaMouseExited

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
                int opcion = JOptionPane.showConfirmDialog(null, "¿Está seguro que desea salir?", "AVISO", JOptionPane.OK_OPTION, JOptionPane.CANCEL_OPTION);
        if (opcion == JOptionPane.OK_OPTION) {
            //guardar();
        }
    }//GEN-LAST:event_formWindowClosing

    public void textoError(java.awt.event.KeyEvent evt, JLabel l) {
        if ('0' <= evt.getKeyChar() && evt.getKeyChar() <= '9') {
            getToolkit().beep();
            l.setText("Ingrese Solamente letras");
            l.setForeground(Color.red);
            evt.consume();
        } else {
            l.setText("");
        }
    }

    public void setearSelectedRButtom(JRadioButton r1, JRadioButton r2, int n) {
        if (n == 1) {
            r1.setSelected(true);
        } else if (n == 2) {
            r2.setSelected(true);
        }
    }

    public boolean verificarCampoVacio(String texto, String nombreCampo) {
        boolean estaVacio = false;
        if (texto.isEmpty()) {
            JOptionPane.showMessageDialog(this, nombreCampo + " no puede estar vacio", "ERROR", JOptionPane.ERROR_MESSAGE);
            estaVacio = true;
        }
        return estaVacio;
    }

    public boolean verificarRegistro() {
        boolean registroOk = false;
        if (!verificarCampoVacio(textFieldNombre.getText(), " El Nombre")) {
            registroOk = true;
        }
        return registroOk;
    }

    public int seleccionRadioButtom(JRadioButton r1, JRadioButton r2) {
        int num = 0;

        if (r1.isSelected()) {
            num = 1;
        } else if (r2.isSelected()) {
            num = 2;
        }
        return num;
    }

    public boolean AgregarPersonaANodoArbol(Elemento<Persona> elemento) {
        boolean ok = false;
        if (verificarRegistro()) {
            String nombre = textFieldNombre.getText();
            String apellido = textFieldApellido.getText();
            String lugar = textFieldLugar.getText();
            String nacionalidad = textFieldNacionalidad.getText();
            int sexo = seleccionRadioButtom(radioButton1Sexo, radioButton2Sexo);
            int estadoSalud = seleccionRadioButtom(radioButtom1EstadoDeSalud, radioButtom2EstadoDeSalud);
            int estadoCivil = comboBoxEstadoCivil.getSelectedIndex();
            String fecha = "";
            String imagen = urlImagen;
            if (dateCalendario.getDate() == null) {
            } else {
                fecha = sdf.format(dateCalendario.getDate());
            }
            Persona p = objSistema.crearPersonaNodoArbol(nombre, apellido, lugar,
                    fecha, nacionalidad, imagen,
                    sexo, estadoCivil, estadoSalud);
            if (tipoPersona == 1) {
                objSistema.getArbolGeneral().insertarPadreRaiz(elemento, p);
                JOptionPane.showMessageDialog(null, "Padre agregado correctamente");

            } else if (tipoPersona == 2) {

                objSistema.getArbolGeneral().setPareja(elemento, p);
                JOptionPane.showMessageDialog(null, "Pareja agregada correctamente");

            } else if (tipoPersona == 3) {
                Elemento<Persona> p2 = objSistema.getArbolGeneral().insertarHijo(elemento, p);
                JOptionPane.showMessageDialog(null, "Hijo agregado correctamente");

            }
            ok = true;
        } else {
            JOptionPane.showMessageDialog(null, "Debes ingresar un nombre");
        }

        return ok;
    }

    public void EditarPersonaEnNodoArbol(Elemento<Persona> elemento) {

        if (verificarRegistro()) {
            elemento.getElemento().setNombre(textFieldNombre.getText());
            elemento.getElemento().setApellido(textFieldApellido.getText());
            elemento.getElemento().setLugar(textFieldLugar.getText());
            elemento.getElemento().setNacionalidad(textFieldNacionalidad.getText());
            elemento.getElemento().setSexo(seleccionRadioButtom(radioButton1Sexo, radioButton2Sexo));
            elemento.getElemento().setEstadoSalud(seleccionRadioButtom(radioButtom1EstadoDeSalud, radioButtom2EstadoDeSalud));
            elemento.getElemento().setEstadiCivil(comboBoxEstadoCivil.getSelectedIndex());
            elemento.getElemento().setImagen(urlImagen);
            String fecha = "";
            if (dateCalendario.getDate() == null) {
            } else {
                fecha = sdf.format(dateCalendario.getDate());
            }
            elemento.getElemento().setFechaNacimiento(fecha);
            JOptionPane.showMessageDialog(null, "Persona editada correctamente");
        }
    }

    public void mostrarArbol() {

        JPanel nuevoArbol = new PanelArbol(objSistema.getArbolGeneral());
        nuevoArbol.setSize(800, 540);
        panelArbol.add(nuevoArbol);
        panelArbol.repaint();

    }

    public void vaciarFormulario() {

        tituloCambiante.setText("Family Tree");
        labelTxtBtnConfirmar.setText("");
        textFieldNombre.setText("");
        textFieldApellido.setText("");
        textFieldLugar.setText("");
        textFieldNacionalidad.setText("");
        radioButtom1EstadoDeSalud.setSelected(false);
        radioButtom2EstadoDeSalud.setSelected(false);
        radioButton1Sexo.setSelected(false);
        radioButton2Sexo.setSelected(false);
        comboBoxEstadoCivil.setSelectedIndex(0);
        dateCalendario.setDate(null);
        ponerFotoPorDefecto();
        textFieldBuscar.setText("");
    }

    public void ponerFotoPorDefecto() {
        ImageIcon imagenProfile = new ImageIcon(getClass().getResource("/Imagenes/addPerson3.png"));
        ImageIcon iconoProfile = new ImageIcon(imagenProfile.getImage().getScaledInstance(labelFotoPersona.getWidth(), labelFotoPersona.getHeight(), Image.SCALE_DEFAULT));
        labelFotoPersona.setIcon(iconoProfile);
    }

    public void cargarImagenDePersona() {;
        if (elementoPersona.getElemento().getImagen() == null) {
            ponerFotoPorDefecto();
        } else {
            urlImagen = elementoPersona.getElemento().getImagen();
            ImageIcon img = new ImageIcon(urlImagen);
            ImageIcon icono = new ImageIcon(img.getImage().getScaledInstance(labelFotoPersona.getWidth(), labelFotoPersona.getHeight(), Image.SCALE_DEFAULT));
            labelFotoPersona.setIcon(icono);
        }
    }

    public void cargarDatosDePersona(Elemento<Persona> elemento) {

        labelTxtBtnConfirmar.setText("Confirmar");
        if (tipoPersona == 2) {
            tituloCambiante.setText("Editar Pareja");
        } else {
            tituloCambiante.setText("Editar Persona");
        }

        textFieldNombre.setText(elemento.getElemento().getNombre());
        textFieldApellido.setText(elemento.getElemento().getApellido());
        textFieldLugar.setText(elemento.getElemento().getLugar());
        textFieldNacionalidad.setText(elemento.getElemento().getNacionalidad());
        setearSelectedRButtom(radioButton1Sexo, radioButton2Sexo, elemento.getElemento().getSexo());
        setearSelectedRButtom(radioButtom1EstadoDeSalud, radioButtom2EstadoDeSalud, elemento.getElemento().getEstadoSalud());
        comboBoxEstadoCivil.setSelectedIndex(elemento.getElemento().getEstadoCivil());
        cargarImagenDePersona();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String dateValue = elemento.getElemento().getFechaNacimiento();
        if (dateValue.equalsIgnoreCase("")) {
            dateCalendario.setDate(null);
        } else {
            try {
                Date date = sdf.parse(dateValue);
                dateCalendario.setDate(date);
            } catch (ParseException ex) {
                Logger.getLogger(VentanaRegistrarPersona.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }


    public void guardar() {
        try {
            FileOutputStream ff = new FileOutputStream("archivo");
            BufferedOutputStream b = new BufferedOutputStream(ff);
            ObjectOutputStream ss = new ObjectOutputStream(b);
            ss.writeObject(objSistema);
            ss.flush();
            ss.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton botonAgregarHijo;
    private javax.swing.JButton botonAgregarPadre;
    private javax.swing.JToggleButton botonAgregarPareja;
    private javax.swing.JToggleButton botonAyuda;
    private javax.swing.JToggleButton botonBorrarPareja;
    private javax.swing.JToggleButton botonBorrarPersona;
    private javax.swing.JButton botonBuscar;
    private javax.swing.JButton botonConfirmar;
    private javax.swing.JButton botonFotoPersona;
    private javax.swing.JToggleButton botonRecuerdos;
    private javax.swing.JButton botonVerArbol;
    private javax.swing.ButtonGroup buttomGroupSexo;
    private javax.swing.ButtonGroup buttonGroupEstadoDeSalud;
    private javax.swing.JComboBox<String> comboBoxEstadoCivil;
    private com.toedter.calendar.JDateChooser dateCalendario;
    private javax.swing.JLabel fondoBlanco;
    private javax.swing.JLabel fondoBlancoImagen;
    private javax.swing.JLabel fondoVerde;
    private javax.swing.JLabel jLabelSeleccionado;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JLabel labeTxtlNombre;
    private javax.swing.JLabel labelBorrarPareja;
    private javax.swing.JLabel labelFotoPersona;
    private javax.swing.JLabel labelTxtAgregarHijo;
    private javax.swing.JLabel labelTxtAgregarPadre;
    private javax.swing.JLabel labelTxtAgregarPareja;
    private javax.swing.JLabel labelTxtApellido;
    private javax.swing.JLabel labelTxtAyuda;
    private javax.swing.JLabel labelTxtBtnConfirmar;
    private javax.swing.JLabel labelTxtBuscar;
    private javax.swing.JLabel labelTxtBuscarError;
    private javax.swing.JLabel labelTxtEliminarPersona;
    private javax.swing.JLabel labelTxtErrorApellido;
    private javax.swing.JLabel labelTxtErrorNacionalidad;
    private javax.swing.JLabel labelTxtErrorNombre;
    private javax.swing.JLabel labelTxtEstadoCivil;
    private javax.swing.JLabel labelTxtEstadoSalud;
    private javax.swing.JLabel labelTxtFechaNacimiento;
    private javax.swing.JLabel labelTxtLugar;
    private javax.swing.JLabel labelTxtNacionalidad;
    private javax.swing.JLabel labelTxtRecuerdos;
    private javax.swing.JLabel labelTxtSeleccionado;
    private javax.swing.JLabel labelTxtSexo;
    private javax.swing.JLabel labelTxtVerArbol;
    private javax.swing.JPanel panelArbol;
    private javax.swing.JPanel panelPersona;
    private javax.swing.JRadioButton radioButtom1EstadoDeSalud;
    private javax.swing.JRadioButton radioButtom2EstadoDeSalud;
    private javax.swing.JRadioButton radioButton1Sexo;
    private javax.swing.JRadioButton radioButton2Sexo;
    private javax.swing.JTextField textFieldApellido;
    private javax.swing.JTextField textFieldBuscar;
    private javax.swing.JTextField textFieldLugar;
    private javax.swing.JTextField textFieldNacionalidad;
    private javax.swing.JTextField textFieldNombre;
    private javax.swing.JLabel tituloCambiante;
    // End of variables declaration//GEN-END:variables
}
