/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import dominio.ArbolGeneral;
import dominio.Elemento;
import dominio.NodoGeneral;
import dominio.Persona;
import dominio.Sistema;
import java.awt.Color;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Rodrigo
 */
public class AgregarRecuerdo extends javax.swing.JFrame {

    private final Sistema objSistema;
    DefaultListModel modelo;
    private ImageIcon imagen;
    private String imagenSinResize;
    private FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivo de imagenes", "jpg");

    /**
     * Creates new form AgregarRecuerdo2
     */
    public AgregarRecuerdo(Sistema unSistema) {
        initComponents();
        modelo = new DefaultListModel();
        objSistema = unSistema;
        this.setLocationRelativeTo(null);
        actualizarVentana();
        getContentPane().setBackground(new Color(0, 153, 204));
    }

    public FileNameExtensionFilter getFilter() {
        return filter;
    }

    public void setFilter(FileNameExtensionFilter filter) {
        this.filter = filter;
    }

    public ImageIcon getImagen() {
        return imagen;
    }

    public void setImagen(ImageIcon unaImgPersona) {
        this.imagen = unaImgPersona;
    }

    public String getImagenSinResize() {
        return imagenSinResize;
    }

    public void setImagenSinResize(String unaImg) {
        this.imagenSinResize = unaImg;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jListPersonas = new javax.swing.JList<>();
        Agregar = new javax.swing.JButton();
        LabelFoto = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        Descripcion = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(0, 153, 204));
        setMaximumSize(new java.awt.Dimension(700, 510));
        setMinimumSize(new java.awt.Dimension(700, 510));
        setPreferredSize(new java.awt.Dimension(700, 510));
        setResizable(false);
        getContentPane().setLayout(null);

        jListPersonas.setToolTipText("");
        jListPersonas.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane1.setViewportView(jListPersonas);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(30, 71, 246, 310);

        Agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar2.png"))); // NOI18N
        Agregar.setBorderPainted(false);
        Agregar.setContentAreaFilled(false);
        Agregar.setFocusPainted(false);
        Agregar.setPreferredSize(new java.awt.Dimension(160, 136));
        Agregar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                AgregarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                AgregarMouseExited(evt);
            }
        });
        Agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarActionPerformed(evt);
            }
        });
        getContentPane().add(Agregar);
        Agregar.setBounds(560, 410, 90, 70);

        LabelFoto.setBackground(new java.awt.Color(255, 255, 255));
        LabelFoto.setOpaque(true);
        getContentPane().add(LabelFoto);
        LabelFoto.setBounds(360, 70, 291, 230);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/add2.png"))); // NOI18N
        jButton1.setBorderPainted(false);
        jButton1.setContentAreaFilled(false);
        jButton1.setFocusPainted(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(600, 300, 64, 40);

        jLabel1.setText("Seleccione Personas:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(30, 29, 150, 24);

        jLabel2.setText("Seleccione imagen:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(360, 30, 120, 14);

        Descripcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DescripcionActionPerformed(evt);
            }
        });
        getContentPane().add(Descripcion);
        Descripcion.setBounds(360, 360, 290, 20);

        jLabel3.setText("Descripción:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(360, 340, 80, 14);

        jLabel4.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Agregar");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(550, 380, 110, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarActionPerformed
        if (jListPersonas.getSelectedValuesList().size() != 0) {
            if (!Descripcion.getText().equalsIgnoreCase("")) {
                if (LabelFoto.getIcon() != null) {
                    List<Persona> listaPersonas = jListPersonas.getSelectedValuesList();
                    ArrayList<Persona> personas = new ArrayList<Persona>();
                    ArbolGeneral arbol = objSistema.getArbolGeneral();
                    NodoGeneral<Elemento<Persona>> nodoGeneral;
                    for (Persona persona : listaPersonas) {

                        personas.add(persona);
                    }
                    objSistema.agregarRecuerdo(getImagenSinResize(), Descripcion.getText(), personas);
                    super.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "Debes seleccionar imagen");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debes ingresar un nombre en la descripción");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debes seleccionar al menos una persona");
        }
    }//GEN-LAST:event_AgregarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        JFileChooser archivoSeleccionado = new JFileChooser();

        archivoSeleccionado.setFileFilter(this.getFilter());
        int opcion = archivoSeleccionado.showOpenDialog(null);
        if (opcion == JFileChooser.APPROVE_OPTION) {

            //url de la imagen
            String file = archivoSeleccionado.getSelectedFile().getPath();
            this.setImagenSinResize(file);
            ImageIcon imageSinResize = new ImageIcon(file);
            this.setImagen(new ImageIcon(imageSinResize.getImage().getScaledInstance(LabelFoto.getWidth(), LabelFoto.getHeight(), Image.SCALE_DEFAULT)));
            LabelFoto.setIcon(this.getImagen());
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void DescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DescripcionActionPerformed

    }//GEN-LAST:event_DescripcionActionPerformed

    private void AgregarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AgregarMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/agregar1.png"));
        Agregar.setIcon(img);
    }//GEN-LAST:event_AgregarMouseEntered

    private void AgregarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AgregarMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/agregar2.png"));
        Agregar.setIcon(img);
    }//GEN-LAST:event_AgregarMouseExited

    public void actualizarVentana() {
        ArbolGeneral arbol = objSistema.getArbolGeneral();
        ArrayList<Elemento<Persona>> personas = arbol.recorridoPreOrden();

        for (int i = 0; i < personas.size(); i++) {
            Elemento<Persona> elemento = personas.get(i);
            Persona p = elemento.getElemento();
            if (arbol.tienePareja(elemento)) {
                modelo.addElement(arbol.obtenerPareja(elemento));
            }
            modelo.addElement(p);
        }
        jListPersonas.setModel(modelo);
        DefaultListCellRenderer cellRenderer = (DefaultListCellRenderer) jListPersonas.getCellRenderer();
        cellRenderer.setHorizontalAlignment(SwingConstants.LEFT);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Agregar;
    private javax.swing.JTextField Descripcion;
    private javax.swing.JLabel LabelFoto;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JList<Persona> jListPersonas;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
