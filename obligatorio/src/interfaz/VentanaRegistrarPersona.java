package interfaz;

import dominio.Sistema;
import java.awt.Color;
import java.awt.Image;
import java.text.SimpleDateFormat;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.filechooser.FileNameExtensionFilter;

public class VentanaRegistrarPersona extends javax.swing.JFrame {

    private Sistema objSistema;
    private FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivo de imagenes", "jpg");
    private ImageIcon imagenPersona;
    private ImageIcon imagenPersonaSinResize;
    private String urlImagen;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public VentanaRegistrarPersona(Sistema modelo) {
        objSistema = modelo;
        initComponents();

        ImageIcon imagenArbol = new ImageIcon(getClass().getResource("/Imagenes/arbolRegistoPersona.png"));
        ImageIcon iconoArbol = new ImageIcon(imagenArbol.getImage().getScaledInstance(fondoRegistrarPersona.getWidth(), fondoRegistrarPersona.getHeight(), Image.SCALE_DEFAULT));
        fondoRegistrarPersona.setIcon(iconoArbol);
        ImageIcon imagenProfile = new ImageIcon(getClass().getResource("/Imagenes/addPerson3.png"));
        ImageIcon iconoProfile = new ImageIcon(imagenProfile.getImage().getScaledInstance(labelFotoPersona.getWidth(), labelFotoPersona.getHeight(), Image.SCALE_DEFAULT));
        labelFotoPersona.setIcon(iconoProfile);
        buttomGroupSexo.add(radioButton1Sexo);
        buttomGroupSexo.add(radioButton2Sexo);
        buttonGroupEstadoDeSalud.add(radioButtom1EstadoDeSalud);
        buttonGroupEstadoDeSalud.add(radioButtom2EstadoDeSalud);
        urlImagen = "/Imagenes/addPerson3.png";
    }

    public FileNameExtensionFilter getFilter() {
        return filter;
    }

    public void setFilter(FileNameExtensionFilter filter) {
        this.filter = filter;
    }

    public ImageIcon getImagenPersona() {
        return imagenPersona;
    }

    public void setImagenPersona(ImageIcon unaImgPersona) {
        this.imagenPersona = unaImgPersona;
    }

    public ImageIcon getImagenPersonaSinResize() {
        return imagenPersonaSinResize;
    }

    public void setImagenPersonaSinResize(ImageIcon unaImgPersona) {
        this.imagenPersonaSinResize = unaImgPersona;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttomGroupSexo = new javax.swing.ButtonGroup();
        buttonGroupEstadoDeSalud = new javax.swing.ButtonGroup();
        panelAgregarPersona = new javax.swing.JPanel();
        titulo = new javax.swing.JLabel();
        dateCalendario = new com.toedter.calendar.JDateChooser();
        botonFotoPersona = new javax.swing.JButton();
        botonConfirmar = new javax.swing.JButton();
        labelTxtBtnConfirmar = new javax.swing.JLabel();
        labelFotoPersona = new javax.swing.JLabel();
        labeTxtlNombre = new javax.swing.JLabel();
        labelTxtApellido = new javax.swing.JLabel();
        labelTxtLugar = new javax.swing.JLabel();
        labelTxtFechaNacimiento = new javax.swing.JLabel();
        labelTxtNacionalidad = new javax.swing.JLabel();
        labelTxtSexo = new javax.swing.JLabel();
        labelTxtEstadoCivil = new javax.swing.JLabel();
        labelTxtEstadoSalud = new javax.swing.JLabel();
        labelTxtErrorNombre = new javax.swing.JLabel();
        labelTxtErrorApellido = new javax.swing.JLabel();
        labelTxtErrorNacionalidad = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        comboBoxEstadoCivil = new javax.swing.JComboBox<>();
        radioButton1Sexo = new javax.swing.JRadioButton();
        radioButton2Sexo = new javax.swing.JRadioButton();
        radioButtom1EstadoDeSalud = new javax.swing.JRadioButton();
        radioButtom2EstadoDeSalud = new javax.swing.JRadioButton();
        textFieldNombre = new javax.swing.JTextField();
        textFieldApellido = new javax.swing.JTextField();
        textFieldLugar = new javax.swing.JTextField();
        textFieldNacionalidad = new javax.swing.JTextField();
        fondoBlancoImagen = new javax.swing.JLabel();
        fondoBlanco = new javax.swing.JLabel();
        fondoAzul = new javax.swing.JLabel();
        fondoVerde = new javax.swing.JLabel();
        fondoRegistrarPersona = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1200, 700));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        panelAgregarPersona.setMinimumSize(new java.awt.Dimension(1200, 700));
        panelAgregarPersona.setLayout(null);

        titulo.setFont(new java.awt.Font("Century Gothic", 2, 40)); // NOI18N
        titulo.setForeground(new java.awt.Color(51, 51, 51));
        titulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titulo.setText("Agrega una Persona");
        panelAgregarPersona.add(titulo);
        titulo.setBounds(670, 0, 490, 60);

        dateCalendario.setDateFormatString("dd/MM/yyyy");
        panelAgregarPersona.add(dateCalendario);
        dateCalendario.setBounds(750, 390, 120, 20);

        botonFotoPersona.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/add.png"))); // NOI18N
        botonFotoPersona.setBorderPainted(false);
        botonFotoPersona.setContentAreaFilled(false);
        botonFotoPersona.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        botonFotoPersona.setFocusPainted(false);
        botonFotoPersona.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                botonFotoPersonaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                botonFotoPersonaMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                botonFotoPersonaMousePressed(evt);
            }
        });
        botonFotoPersona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonFotoPersonaActionPerformed(evt);
            }
        });
        panelAgregarPersona.add(botonFotoPersona);
        botonFotoPersona.setBounds(950, 150, 40, 40);

        botonConfirmar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/tick3.png"))); // NOI18N
        botonConfirmar.setBorderPainted(false);
        botonConfirmar.setContentAreaFilled(false);
        botonConfirmar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        botonConfirmar.setFocusable(false);
        botonConfirmar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                botonConfirmarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                botonConfirmarMouseExited(evt);
            }
        });
        botonConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonConfirmarActionPerformed(evt);
            }
        });
        panelAgregarPersona.add(botonConfirmar);
        botonConfirmar.setBounds(1030, 620, 40, 40);

        labelTxtBtnConfirmar.setBackground(new java.awt.Color(255, 255, 255));
        labelTxtBtnConfirmar.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        labelTxtBtnConfirmar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTxtBtnConfirmar.setText("Confirmar");
        labelTxtBtnConfirmar.setOpaque(true);
        panelAgregarPersona.add(labelTxtBtnConfirmar);
        labelTxtBtnConfirmar.setBounds(1000, 610, 90, 20);

        labelFotoPersona.setBackground(new java.awt.Color(255, 255, 255));
        labelFotoPersona.setOpaque(true);
        panelAgregarPersona.add(labelFotoPersona);
        labelFotoPersona.setBounds(870, 70, 100, 100);

        labeTxtlNombre.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labeTxtlNombre.setForeground(new java.awt.Color(51, 51, 51));
        labeTxtlNombre.setText("Nombre");
        panelAgregarPersona.add(labeTxtlNombre);
        labeTxtlNombre.setBounds(750, 190, 50, 20);

        labelTxtApellido.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtApellido.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtApellido.setText("Apellido");
        panelAgregarPersona.add(labelTxtApellido);
        labelTxtApellido.setBounds(750, 250, 50, 14);

        labelTxtLugar.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtLugar.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtLugar.setText("Lugar");
        panelAgregarPersona.add(labelTxtLugar);
        labelTxtLugar.setBounds(750, 310, 40, 14);

        labelTxtFechaNacimiento.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtFechaNacimiento.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtFechaNacimiento.setText("Fecha de Nacimiento");
        panelAgregarPersona.add(labelTxtFechaNacimiento);
        labelTxtFechaNacimiento.setBounds(750, 370, 140, 14);

        labelTxtNacionalidad.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtNacionalidad.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtNacionalidad.setText("Nacionalidad");
        panelAgregarPersona.add(labelTxtNacionalidad);
        labelTxtNacionalidad.setBounds(750, 430, 90, 14);

        labelTxtSexo.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtSexo.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtSexo.setText("Sexo");
        panelAgregarPersona.add(labelTxtSexo);
        labelTxtSexo.setBounds(750, 550, 30, 14);

        labelTxtEstadoCivil.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtEstadoCivil.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtEstadoCivil.setText("Estado Civil");
        panelAgregarPersona.add(labelTxtEstadoCivil);
        labelTxtEstadoCivil.setBounds(750, 490, 70, 14);

        labelTxtEstadoSalud.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        labelTxtEstadoSalud.setForeground(new java.awt.Color(51, 51, 51));
        labelTxtEstadoSalud.setText("Estado de Salud");
        panelAgregarPersona.add(labelTxtEstadoSalud);
        labelTxtEstadoSalud.setBounds(750, 610, 100, 14);

        labelTxtErrorNombre.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        panelAgregarPersona.add(labelTxtErrorNombre);
        labelTxtErrorNombre.setBounds(810, 190, 180, 20);

        labelTxtErrorApellido.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        panelAgregarPersona.add(labelTxtErrorApellido);
        labelTxtErrorApellido.setBounds(810, 240, 180, 30);

        labelTxtErrorNacionalidad.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        panelAgregarPersona.add(labelTxtErrorNacionalidad);
        labelTxtErrorNacionalidad.setBounds(840, 420, 180, 30);
        panelAgregarPersona.add(jSeparator1);
        jSeparator1.setBounds(750, 240, 330, 10);
        panelAgregarPersona.add(jSeparator2);
        jSeparator2.setBounds(750, 300, 330, 10);
        panelAgregarPersona.add(jSeparator3);
        jSeparator3.setBounds(750, 360, 330, 10);
        panelAgregarPersona.add(jSeparator4);
        jSeparator4.setBounds(750, 420, 330, 10);
        panelAgregarPersona.add(jSeparator5);
        jSeparator5.setBounds(750, 480, 330, 10);
        panelAgregarPersona.add(jSeparator6);
        jSeparator6.setBounds(750, 600, 330, 10);
        panelAgregarPersona.add(jSeparator7);
        jSeparator7.setBounds(750, 540, 330, 10);

        comboBoxEstadoCivil.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        comboBoxEstadoCivil.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Sin Determinar", "Soltero", "Casado", "Divorciado", "Viudo", "En Pareja" }));
        comboBoxEstadoCivil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxEstadoCivilActionPerformed(evt);
            }
        });
        panelAgregarPersona.add(comboBoxEstadoCivil);
        comboBoxEstadoCivil.setBounds(750, 510, 120, 22);

        radioButton1Sexo.setBackground(new java.awt.Color(255, 255, 255));
        radioButton1Sexo.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        radioButton1Sexo.setText("M");
        radioButton1Sexo.setFocusPainted(false);
        radioButton1Sexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButton1SexoActionPerformed(evt);
            }
        });
        panelAgregarPersona.add(radioButton1Sexo);
        radioButton1Sexo.setBounds(750, 570, 40, 20);

        radioButton2Sexo.setBackground(new java.awt.Color(255, 255, 255));
        radioButton2Sexo.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        radioButton2Sexo.setText("F");
        radioButton2Sexo.setFocusPainted(false);
        radioButton2Sexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioButton2SexoActionPerformed(evt);
            }
        });
        panelAgregarPersona.add(radioButton2Sexo);
        radioButton2Sexo.setBounds(810, 570, 40, 20);

        radioButtom1EstadoDeSalud.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        radioButtom1EstadoDeSalud.setText("Vivo");
        radioButtom1EstadoDeSalud.setOpaque(false);
        panelAgregarPersona.add(radioButtom1EstadoDeSalud);
        radioButtom1EstadoDeSalud.setBounds(750, 630, 60, 25);

        radioButtom2EstadoDeSalud.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        radioButtom2EstadoDeSalud.setText("Difunto");
        radioButtom2EstadoDeSalud.setOpaque(false);
        panelAgregarPersona.add(radioButtom2EstadoDeSalud);
        radioButtom2EstadoDeSalud.setBounds(810, 630, 93, 25);

        textFieldNombre.setBorder(null);
        textFieldNombre.setOpaque(false);
        textFieldNombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                textFieldNombreMouseClicked(evt);
            }
        });
        textFieldNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldNombreActionPerformed(evt);
            }
        });
        textFieldNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textFieldNombreKeyTyped(evt);
            }
        });
        panelAgregarPersona.add(textFieldNombre);
        textFieldNombre.setBounds(750, 210, 330, 30);

        textFieldApellido.setBackground(new java.awt.Color(102, 255, 102));
        textFieldApellido.setBorder(null);
        textFieldApellido.setOpaque(false);
        textFieldApellido.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                textFieldApellidoMouseClicked(evt);
            }
        });
        textFieldApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textFieldApellidoKeyTyped(evt);
            }
        });
        panelAgregarPersona.add(textFieldApellido);
        textFieldApellido.setBounds(750, 270, 330, 30);

        textFieldLugar.setBorder(null);
        textFieldLugar.setOpaque(false);
        textFieldLugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldLugarActionPerformed(evt);
            }
        });
        panelAgregarPersona.add(textFieldLugar);
        textFieldLugar.setBounds(750, 330, 330, 30);

        textFieldNacionalidad.setBorder(null);
        textFieldNacionalidad.setOpaque(false);
        textFieldNacionalidad.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                textFieldNacionalidadMouseClicked(evt);
            }
        });
        textFieldNacionalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textFieldNacionalidadKeyTyped(evt);
            }
        });
        panelAgregarPersona.add(textFieldNacionalidad);
        textFieldNacionalidad.setBounds(750, 450, 330, 30);

        fondoBlancoImagen.setBackground(new java.awt.Color(255, 255, 255));
        fondoBlancoImagen.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        fondoBlancoImagen.setOpaque(true);
        panelAgregarPersona.add(fondoBlancoImagen);
        fondoBlancoImagen.setBounds(850, 60, 140, 130);

        fondoBlanco.setBackground(new java.awt.Color(255, 255, 255));
        fondoBlanco.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        fondoBlanco.setOpaque(true);
        panelAgregarPersona.add(fondoBlanco);
        fondoBlanco.setBounds(730, 130, 380, 530);

        fondoAzul.setBackground(new java.awt.Color(0, 153, 204));
        fondoAzul.setOpaque(true);
        panelAgregarPersona.add(fondoAzul);
        fondoAzul.setBounds(630, 0, 570, 270);

        fondoVerde.setBackground(new java.awt.Color(152, 222, 142));
        fondoVerde.setOpaque(true);
        panelAgregarPersona.add(fondoVerde);
        fondoVerde.setBounds(630, 270, 570, 430);
        panelAgregarPersona.add(fondoRegistrarPersona);
        fondoRegistrarPersona.setBounds(0, 0, 630, 700);

        getContentPane().add(panelAgregarPersona);
        panelAgregarPersona.setBounds(0, 0, 1200, 700);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        int opcion = JOptionPane.showConfirmDialog(null, "¿Está seguro que desea salir?", "AVISO", JOptionPane.OK_OPTION, JOptionPane.CANCEL_OPTION);
        if (opcion == JOptionPane.OK_OPTION) {

        }
    }//GEN-LAST:event_formWindowClosing

    private void textFieldNacionalidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldNacionalidadKeyTyped
        textoError(evt, labelTxtErrorNacionalidad);
    }//GEN-LAST:event_textFieldNacionalidadKeyTyped

    private void textFieldNacionalidadMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textFieldNacionalidadMouseClicked
        labelTxtErrorNacionalidad.setText("");
        labelTxtErrorApellido.setText("");
        labelTxtErrorNombre.setText("");
    }//GEN-LAST:event_textFieldNacionalidadMouseClicked

    private void textFieldLugarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldLugarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldLugarActionPerformed

    private void textFieldApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldApellidoKeyTyped
        textoError(evt, labelTxtErrorApellido);
    }//GEN-LAST:event_textFieldApellidoKeyTyped

    private void textFieldApellidoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textFieldApellidoMouseClicked
        labelTxtErrorApellido.setText("");
        labelTxtErrorNombre.setText("");
        labelTxtErrorNacionalidad.setText("");
    }//GEN-LAST:event_textFieldApellidoMouseClicked

    private void textFieldNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textFieldNombreKeyTyped
        textoError(evt, labelTxtErrorNombre);
    }//GEN-LAST:event_textFieldNombreKeyTyped

    private void textFieldNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldNombreActionPerformed

    private void textFieldNombreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textFieldNombreMouseClicked
        labelTxtErrorNombre.setText("");
        labelTxtErrorApellido.setText("");
        labelTxtErrorNacionalidad.setText("");
    }//GEN-LAST:event_textFieldNombreMouseClicked

    private void radioButton2SexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButton2SexoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radioButton2SexoActionPerformed

    private void radioButton1SexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioButton1SexoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radioButton1SexoActionPerformed

    private void comboBoxEstadoCivilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxEstadoCivilActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboBoxEstadoCivilActionPerformed

    private void botonConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonConfirmarActionPerformed
        if (registrarPersona()) {
            VentanaPrincipal v = new VentanaPrincipal(objSistema);
            v.setVisible(true);
            dispose();
        }

    }//GEN-LAST:event_botonConfirmarActionPerformed

    private void botonConfirmarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonConfirmarMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/tick3.png"));
        botonConfirmar.setIcon(img);
    }//GEN-LAST:event_botonConfirmarMouseExited

    private void botonConfirmarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonConfirmarMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/tick.png"));
        botonConfirmar.setIcon(img);
    }//GEN-LAST:event_botonConfirmarMouseEntered

    private void botonFotoPersonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonFotoPersonaActionPerformed
        JFileChooser archivoSeleccionado = new JFileChooser();

        archivoSeleccionado.setFileFilter(this.getFilter());
        int opcion = archivoSeleccionado.showOpenDialog(null);
        if (opcion == JFileChooser.APPROVE_OPTION) {

            //url de la imagen
            String file = archivoSeleccionado.getSelectedFile().getPath();
            urlImagen = file;
            this.setImagenPersonaSinResize(new ImageIcon(file));
            this.setImagenPersona(new ImageIcon(this.getImagenPersonaSinResize().getImage().getScaledInstance(labelFotoPersona.getWidth(), labelFotoPersona.getHeight(), Image.SCALE_DEFAULT)));
            labelFotoPersona.setIcon(this.getImagenPersona());
        }
    }//GEN-LAST:event_botonFotoPersonaActionPerformed

    private void botonFotoPersonaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonFotoPersonaMousePressed
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/add.png"));
        botonFotoPersona.setIcon(img);
    }//GEN-LAST:event_botonFotoPersonaMousePressed

    private void botonFotoPersonaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonFotoPersonaMouseExited
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/add.png"));
        botonFotoPersona.setIcon(img);
    }//GEN-LAST:event_botonFotoPersonaMouseExited

    private void botonFotoPersonaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonFotoPersonaMouseEntered
        ImageIcon img = new ImageIcon(getClass().getResource("/Imagenes/add2.png"));
        botonFotoPersona.setIcon(img);
    }//GEN-LAST:event_botonFotoPersonaMouseEntered

    public void textoError(java.awt.event.KeyEvent evt, JLabel l) {
        if ('0' <= evt.getKeyChar() && evt.getKeyChar() <= '9') {
            getToolkit().beep();
            l.setText("Ingrese Solamente letras");
            l.setForeground(Color.red);
            evt.consume();
        } else {
            l.setText("");
        }
    }

    public boolean verificarCampoVacio(String texto, String nombreCampo) {
        boolean estaVacio = false;
        if (texto.isEmpty()) {
            JOptionPane.showMessageDialog(this, nombreCampo + " no puede estar vacio", "ERROR", JOptionPane.ERROR_MESSAGE);
            estaVacio = true;
        }
        return estaVacio;
    }

    public boolean verificarRegistro() {
        boolean registroOk = false;
        if (!verificarCampoVacio(textFieldNombre.getText(), " El Nombre")) {
            registroOk = true;
        }
        return registroOk;
    }

    public int seleccionRadioButtom(JRadioButton r1, JRadioButton r2) {
        int num = 0;

        if (r1.isSelected()) {
            num = 1;
        } else if (r2.isSelected()) {
            num = 2;
        }
        return num;
    }

    public boolean registrarPersona() {
        boolean ok = false;
        if (verificarRegistro()) {

            String nombre = textFieldNombre.getText();
            String apellido = textFieldApellido.getText();
            String lugar = textFieldLugar.getText();
            String nacionalidad = textFieldNacionalidad.getText();
            int sexo = seleccionRadioButtom(radioButton1Sexo, radioButton2Sexo);
            int estadoSalud = seleccionRadioButtom(radioButtom1EstadoDeSalud, radioButtom2EstadoDeSalud);
            int estadoCivil = comboBoxEstadoCivil.getSelectedIndex();
            String imagen = urlImagen;
            String fecha;
            if (dateCalendario.getDate() == null) {
                fecha = "";
            } else {
                fecha = sdf.format(dateCalendario.getDate());
            }
            objSistema.agregarPrimeraPersonaArbol(nombre, apellido, lugar,
                    fecha, nacionalidad, imagen,
                    sexo, estadoCivil, estadoSalud);
            ok = true;
        }
        return ok;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonConfirmar;
    private javax.swing.JButton botonFotoPersona;
    private javax.swing.ButtonGroup buttomGroupSexo;
    private javax.swing.ButtonGroup buttonGroupEstadoDeSalud;
    private javax.swing.JComboBox<String> comboBoxEstadoCivil;
    private com.toedter.calendar.JDateChooser dateCalendario;
    private javax.swing.JLabel fondoAzul;
    private javax.swing.JLabel fondoBlanco;
    private javax.swing.JLabel fondoBlancoImagen;
    private javax.swing.JLabel fondoRegistrarPersona;
    private javax.swing.JLabel fondoVerde;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JLabel labeTxtlNombre;
    private javax.swing.JLabel labelFotoPersona;
    private javax.swing.JLabel labelTxtApellido;
    private javax.swing.JLabel labelTxtBtnConfirmar;
    private javax.swing.JLabel labelTxtErrorApellido;
    private javax.swing.JLabel labelTxtErrorNacionalidad;
    private javax.swing.JLabel labelTxtErrorNombre;
    private javax.swing.JLabel labelTxtEstadoCivil;
    private javax.swing.JLabel labelTxtEstadoSalud;
    private javax.swing.JLabel labelTxtFechaNacimiento;
    private javax.swing.JLabel labelTxtLugar;
    private javax.swing.JLabel labelTxtNacionalidad;
    private javax.swing.JLabel labelTxtSexo;
    private javax.swing.JPanel panelAgregarPersona;
    private javax.swing.JRadioButton radioButtom1EstadoDeSalud;
    private javax.swing.JRadioButton radioButtom2EstadoDeSalud;
    private javax.swing.JRadioButton radioButton1Sexo;
    private javax.swing.JRadioButton radioButton2Sexo;
    private javax.swing.JTextField textFieldApellido;
    private javax.swing.JTextField textFieldLugar;
    private javax.swing.JTextField textFieldNacionalidad;
    private javax.swing.JTextField textFieldNombre;
    private javax.swing.JLabel titulo;
    // End of variables declaration//GEN-END:variables
}
