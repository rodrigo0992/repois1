package interfaz;

import dominio.ArbolGeneral;
import dominio.Elemento;
import dominio.NodoGeneral;
import dominio.Persona;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JPanel;

/**
 * Creado por hadexexplade el 01 de febrero del 2016
 *
 * @param <E>
 */
public class PanelArbol<E> extends JPanel {

    private int GAP_X = 10;
    private int ALTO_NODO = 50;
    private int ANCHO_NODO = 110;
    private int GAP_Y = 10;

    private ArbolGeneral<E> arbolGeneral;

    public PanelArbol(ArbolGeneral<E> arbolGeneral) {
        this.arbolGeneral = arbolGeneral;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (!arbolGeneral.estaVacia()) {

            BufferedImage myPicture = dibujarArbol(arbolGeneral.getRaiz());
            g.drawImage(myPicture, 0, 0, null);

        }
    }

    public BufferedImage dibujarArbol(NodoGeneral<E> raiz) {
        if (raiz.cantHijos() == 0) {
            return dibujarNodo(raiz);
        }

        ArrayList<BufferedImage> imagenes = new ArrayList<BufferedImage>();
        BufferedImage imagenFinal;
        int ancho = 0, alto = 0;
        for (Object hijo : raiz.hijos()) {
            NodoGeneral<E> son = (NodoGeneral<E>) hijo;

            BufferedImage im = dibujarArbol(son);
            imagenes.add(im);
            ancho += im.getWidth() + GAP_X;

            if (im.getHeight() > alto) {
                alto = im.getHeight();
            }
        }
        alto += ALTO_NODO + 2 * GAP_Y;
        BufferedImage imagenRaiz = dibujarNodo(raiz);
        if (raiz.getPareja() != null) {
            ancho += imagenRaiz.getWidth();
        }

        if (ancho < imagenRaiz.getHeight()) {
            ancho = imagenRaiz.getHeight();
        }

        imagenFinal = new BufferedImage(ancho, alto, BufferedImage.TYPE_INT_RGB);
        imagenFinal.getGraphics().setColor(Color.BLACK);
        imagenFinal.getGraphics().fillRect(0, 0, getWidth(), getHeight());
        Graphics2D g2d = imagenFinal.createGraphics();

        //Se imprime la raiz y su pareja si corresponde
        g2d.drawImage(imagenRaiz, ancho / 2 - ANCHO_NODO / 2, 0, null);

        //se imprimen hijos 
        int posx = GAP_X;

        for (BufferedImage bNodo : imagenes) {
            g2d.setColor(new Color(186, 103, 109));
            g2d.setStroke(new BasicStroke(2));
            g2d.drawLine(ancho / 2, ALTO_NODO + 0, posx + bNodo.getWidth() / 3, ALTO_NODO + GAP_Y * 2);
            g2d.drawImage(bNodo, posx, ALTO_NODO + 2 * GAP_Y, this);
            posx += bNodo.getWidth() + GAP_X;
        }

        imagenFinal.getGraphics().setColor(Color.BLACK);
        imagenFinal.getGraphics().drawRect(0, 0, getWidth(), getHeight());
        return imagenFinal;
    }

    /**
     * netodo para dibujar un solo nodo
     *
     * @param n el nodo a dibujar
     * @return una imagen con le nodo dibujado
     */
    private BufferedImage dibujarNodo(NodoGeneral<E> n) {
        BufferedImage nodoFinal;
        BufferedImage imagenNodo;

        if (n.getPareja() != null) {

            nodoFinal = new BufferedImage(ANCHO_NODO * 2 + GAP_X, ALTO_NODO, BufferedImage.TYPE_INT_RGB);
            nodoFinal.getGraphics().setColor(Color.BLACK);
            nodoFinal.getGraphics().fillRect(0, 0, getWidth(), getHeight());
            Graphics2D g2d = nodoFinal.createGraphics();

            imagenNodo = dibujarNodoSimple(n);
            BufferedImage imagenNodoPareja = dibujarPareja(n.getPareja());

            g2d.setColor(new Color(186, 103, 109));
            g2d.setStroke(new BasicStroke(2));
            g2d.drawLine(ANCHO_NODO, ALTO_NODO / 2, ANCHO_NODO + GAP_X, ALTO_NODO / 2);
            g2d.drawImage(imagenNodo, 0, 0, null);
            g2d.drawImage(imagenNodoPareja, imagenNodo.getWidth() + GAP_X, 0, null);

            return nodoFinal;

        } else {

            return dibujarNodoSimple(n);

        }

    }

    private BufferedImage dibujarNodoSimple(NodoGeneral<E> n) {
        BufferedImage imagenNodo = new BufferedImage(ANCHO_NODO, ALTO_NODO, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = imagenNodo.createGraphics();

        g.setColor(new Color(0, 153, 204));
        g.fillRect(0, 0, ANCHO_NODO, ALTO_NODO);

        BasicStroke bs = new BasicStroke(3);
        g.setStroke(bs);
        g.setColor(new Color(186, 103, 109));
        g.drawRect(0, 0, ANCHO_NODO - 1, ALTO_NODO - 1);

        g.setColor(Color.WHITE);
        g.setFont(new Font("Arial", Font.BOLD, 20));
        Persona p = (Persona) n.getElemento();
        g.drawString(p.getNombre(), 10, 20);
        g.drawString(p.getApellido(), 10, 43);
        g.setColor(new Color(102, 255, 102));
        g.drawString(String.valueOf(n.getNodoId()), 95, 18);

        return imagenNodo;
    }

    private BufferedImage dibujarPareja(E e) {
        BufferedImage imagenNodo = new BufferedImage(ANCHO_NODO, ALTO_NODO, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = imagenNodo.createGraphics();

        g.setColor(new Color(0, 153, 204));
        g.fillRect(0, 0, ANCHO_NODO, ALTO_NODO);

        BasicStroke bs = new BasicStroke(3);
        g.setStroke(bs);
        g.setColor(new Color(186, 103, 109));
        g.drawRect(0, 0, ANCHO_NODO - 1, ALTO_NODO - 1);

        g.setColor(Color.WHITE);
        g.setFont(new Font("Arial", Font.BOLD, 20));
        Persona p = (Persona) e;
        g.drawString(p.getNombre(), 10, 20);
        g.drawString(p.getApellido(), 10, 43);

        return imagenNodo;
    }

}
