/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rodrigo
 */
public class SistemaTest {

    Sistema sistema;

    public SistemaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        sistema = new Sistema();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getRecuerdos method, of class Sistema.
     */
    @Test
    public void testGetRecuerdos() {
        String imagen = "/Imagenes/addPerson3.png";
        String descripcion = "imagen";
        Persona persona = new Persona("Rodrigo", "Duarte", "Montevideo", "09/11/1992", "Uruguayo", "/Imagenes/addPerson3.png", 1, 1, 1);
        Persona persona2 = new Persona("Sebastian", "Colina", "Montevideo", "09/11/1992", "Uruguayo", "/Imagenes/addPerson3.png", 1, 1, 1);
        ArrayList<Persona> personas = new ArrayList<Persona>();
        personas.add(persona);
        personas.add(persona2);
        Recuerdo recuerdo = new Recuerdo(imagen, descripcion, personas);
        sistema.agregarRecuerdo(imagen, descripcion, personas);

        ArrayList<Recuerdo> recuerdosSistema = sistema.getRecuerdos();
        Recuerdo recEncontrado = null;
        for (Recuerdo r : recuerdosSistema) {
            recEncontrado = r;
        }
        assertEquals(descripcion, recEncontrado.getDescripcion());
        assertEquals(imagen, recEncontrado.getImagen());
        assertEquals(personas, recEncontrado.getPersonas());
    }

    /**
     * Test of agregarRecuerdo method, of class Sistema.
     */
    @Test
    public void testAgregarRecuerdo() {
        String imagen = "/Imagenes/addPerson3.png";
        String descripcion = "imagen";
        Persona persona = new Persona("Rodrigo", "Duarte", "Montevideo", "09/11/1992", "Uruguayo", "/Imagenes/addPerson3.png", 1, 1, 1);
        Persona persona2 = new Persona("Sebastian", "Colina", "Montevideo", "09/11/1992", "Uruguayo", "/Imagenes/addPerson3.png", 1, 1, 1);
        ArrayList<Persona> personas = new ArrayList<Persona>();
        personas.add(persona);
        personas.add(persona2);
        Recuerdo recSistema = null;
        sistema.agregarRecuerdo(imagen, descripcion, personas);
        for (Recuerdo r : sistema.getRecuerdos()) {
            recSistema = r;
        }
        assertEquals(descripcion, recSistema.getDescripcion());
        assertEquals(imagen, recSistema.getImagen());
        assertEquals(personas, recSistema.getPersonas());
    }

    /**
     * Test of getRecuerdo method, of class Sistema.
     */
    @Test
    public void testGetRecuerdo() {
        String imagen = "/Imagenes/addPerson3.png";
        String descripcion = "imagen";
        Persona persona = new Persona("Rodrigo", "Duarte", "Montevideo", "09/11/1992", "Uruguayo", "/Imagenes/addPerson3.png", 1, 1, 1);
        Persona persona2 = new Persona("Sebastian", "Colina", "Montevideo", "09/11/1992", "Uruguayo", "/Imagenes/addPerson3.png", 1, 1, 1);
        ArrayList<Persona> personas = new ArrayList<Persona>();
        personas.add(persona);
        personas.add(persona2);
        sistema.agregarRecuerdo(imagen, descripcion, personas);

        assertEquals(imagen, sistema.getRecuerdo(descripcion).getImagen());
        assertEquals(descripcion, sistema.getRecuerdo(descripcion).getDescripcion());
        assertEquals(personas, sistema.getRecuerdo(descripcion).getPersonas());

    }

    /**
     * Test of agregarPrimeraPersonaArbol method, of class Sistema.
     */
    @Test
    public void testAgregarPrimeraPersonaArbolyGetArbolGeneral() {
        sistema.agregarPrimeraPersonaArbol("Rodrigo", "Duarte", "Montevideo", "09/11/1992", "Uruguayo", "imagenes/imagen.png", 1, 1, 1);
        Persona raiz = sistema.getArbolGeneral().getRaiz().getElemento();

        assertEquals("Rodrigo", raiz.getNombre());
        assertEquals("Duarte", raiz.getApellido());
        assertEquals("Montevideo", raiz.getLugar());
        assertEquals("09/11/1992", raiz.getFechaNacimiento());
        assertEquals("Uruguayo", raiz.getNacionalidad());
        assertEquals("imagenes/imagen.png", raiz.getImagen());
        assertEquals(1, raiz.getSexo());
        assertEquals(1, raiz.getEstadoCivil());
        assertEquals(1, raiz.getEstadoSalud());
    }

    /**
     * Test of crearPersonaNodoArbol method, of class Sistema.
     */
    @Test
    public void testCrearPersonaNodoArbol() {
        Persona persona = sistema.crearPersonaNodoArbol("Rodrigo", "Duarte", "Montevideo", "09/11/1992", "Uruguayo", "imagenes/imagen.png", 1, 1, 1);

        assertEquals("Rodrigo", persona.getNombre());
        assertEquals("Duarte", persona.getApellido());
        assertEquals("Montevideo", persona.getLugar());
        assertEquals("09/11/1992", persona.getFechaNacimiento());
        assertEquals("Uruguayo", persona.getNacionalidad());
        assertEquals("imagenes/imagen.png", persona.getImagen());
        assertEquals(1, persona.getSexo());
        assertEquals(1, persona.getEstadoCivil());
        assertEquals(1, persona.getEstadoSalud());

    }

}
