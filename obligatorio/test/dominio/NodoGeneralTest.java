/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rodrigo
 */
public class NodoGeneralTest {

    public NodoGeneralTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getElemento method, of class NodoGeneral.
     */
    @Test
    public void testGetElemento() {

        Persona persona = new Persona();
        persona.setNombre("Rodrigo");
        NodoGeneral<Persona> nodo = new NodoGeneral(persona, null, 1);

        assertEquals(persona, nodo.getElemento());
    }

    /**
     * Test of getPadre method, of class NodoGeneral.
     */
    @Test
    public void testGetPadre() {
        Persona padre = new Persona();
        padre.setNombre("Padre");
        NodoGeneral<Persona> nodoPadre = new NodoGeneral(padre, null, 1);
        Persona hijo = new Persona();
        hijo.setNombre("Hijo");
        NodoGeneral<Persona> nodo = new NodoGeneral(hijo, nodoPadre, 1);

        assertEquals(nodoPadre, nodo.getPadre());
    }

    /**
     * Test of setPadre method, of class NodoGeneral.
     */
    @Test
    public void testSetPadre() {
        Persona padre = new Persona();
        padre.setNombre("Padre");
        NodoGeneral<Persona> nodoPadre = new NodoGeneral(padre, null, 1);
        Persona hijo = new Persona();
        hijo.setNombre("Hijo");
        NodoGeneral<Persona> nodo = new NodoGeneral(hijo, null, 1);
        nodo.setPadre(nodoPadre);

        assertEquals(nodoPadre, nodo.getPadre());

    }

    /**
     * Test of setPareja method, of class NodoGeneral.
     */
    @Test
    public void testSetPareja() {
        Persona persona = new Persona();
        persona.setNombre("Rodrigo");
        Persona pareja = new Persona();
        pareja.setNombre("Pareja");
        NodoGeneral<Persona> nodo = new NodoGeneral(persona, null, 1);
        nodo.setPareja(pareja);

        assertEquals(pareja, nodo.getPareja());

    }

//    /**
//     * Test of getPareja method, of class NodoGeneral.
//     */
    @Test
    public void testGetPareja() {
        Persona persona = new Persona();
        persona.setNombre("Rodrigo");
        NodoGeneral<Persona> nodo = new NodoGeneral(persona, null, 1);
        Persona pareja = new Persona();
        persona.setNombre("Pareja");
        nodo.setPareja(pareja);

        assertEquals(pareja, nodo.getPareja());

    }

//    /**
//     * Test of insertarHijo method, of class NodoGeneral.
//     */
    @Test
    public void testInsertarHijo() {
        Persona persona = new Persona();
        persona.setNombre("Rodrigo");
        NodoGeneral<Persona> nodo = new NodoGeneral(persona, null, 1);
        Persona hijo = new Persona();
        hijo.setNombre("Hijo");
        NodoGeneral<Persona> nodoHijo = new NodoGeneral(hijo, nodo, 2);
        nodo.insertarHijo(nodoHijo);
        Elemento<Persona> elemEncontrado = null;
        for (Elemento<Persona> elem : nodo.hijos()) {
            elemEncontrado = elem;
        }
        assertEquals(nodoHijo, elemEncontrado);
    }

    /**
     * Test of hijos method, of class NodoGeneral.
     */
    @Test
    public void testHijos() {
        ArrayList<Elemento<Persona>> hijos = new ArrayList<Elemento<Persona>>();
        Persona persona = new Persona();
        persona.setNombre("Padre");
        NodoGeneral<Persona> nodoPadre = new NodoGeneral(persona, null, 1);
        persona = new Persona();
        persona.setNombre("Rodrigo");
        NodoGeneral<Persona> hijo1 = new NodoGeneral(persona, nodoPadre, 2);
        hijos.add(hijo1);
        nodoPadre.insertarHijo(hijo1);
        persona = new Persona();
        persona.setNombre("Sebastian");
        NodoGeneral<Persona> hijo2 = new NodoGeneral(persona, nodoPadre, 3);
        hijos.add(hijo2);
        nodoPadre.insertarHijo(hijo2);

        assertEquals(hijos, nodoPadre.hijos());
    }
//
//    /**
//     * Test of getNodoId method, of class NodoGeneral.
//     */

    @Test
    public void testGetNodoId() {
        Persona persona = new Persona();
        persona.setNombre("Rodrigo");
        NodoGeneral<Persona> hijo1 = new NodoGeneral(persona, null, 2);

        assertEquals(2, hijo1.getNodoId());
    }
}
