/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rodrigo
 */
public class RecuerdoTest {

    Recuerdo recuerdo;

    public RecuerdoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        String imagen = "/Imagenes/addPerson3.png";
        String descripcion = "imagen";
        Persona persona = new Persona("Rodrigo", "Duarte", "Montevideo", "09/11/1992", "Uruguayo", "/Imagenes/addPerson3.png", 1, 1, 1);
        Persona persona2 = new Persona("Sebastian", "Colina", "Montevideo", "09/11/1992", "Uruguayo", "/Imagenes/addPerson3.png", 1, 1, 1);
        ArrayList<Persona> personas = new ArrayList<Persona>();
        personas.add(persona);
        personas.add(persona2);
        recuerdo = new Recuerdo(imagen, descripcion, personas);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getImagen method, of class Recuerdo.
     */
    @Test
    public void testGetImagen() {
        assertEquals("/Imagenes/addPerson3.png", recuerdo.getImagen());
    }

    /**
     * Test of setImagen method, of class Recuerdo.
     */
    @Test
    public void testSetImagen() {
        recuerdo.setImagen("/Imagenes/imagenPrueba.png");
        assertEquals("/Imagenes/imagenPrueba.png", recuerdo.getImagen());

    }

    /**
     * Test of getDescripcion method, of class Recuerdo.
     */
    @Test
    public void testGetDescripcion() {
        assertEquals("imagen", recuerdo.getDescripcion());
    }

    /**
     * Test of setDescripcion method, of class Recuerdo.
     */
    @Test
    public void testSetDescripcion() {
        recuerdo.setDescripcion("descripcionPrueba");
        assertEquals("descripcionPrueba", recuerdo.getDescripcion());
    }

    /**
     * Test of getPersonas method, of class Recuerdo.
     */
    @Test
    public void testGetPersonas() {
        Persona persona = new Persona("Rodrigo", "Duarte", "Montevideo", "09/11/1992", "Uruguayo", "/Imagenes/addPerson3.png", 1, 1, 1);
        Persona persona2 = new Persona("Sebastian", "Colina", "Montevideo", "09/11/1992", "Uruguayo", "/Imagenes/addPerson3.png", 1, 1, 1);
        ArrayList<Persona> personas = new ArrayList<Persona>();
        personas.add(persona);
        personas.add(persona2);
        Recuerdo rec = new Recuerdo("/Imagenes/addPerson3.png", "Imagen", personas);

        assertEquals(personas, rec.getPersonas());
    }

}
