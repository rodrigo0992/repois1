/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rodrigo
 */
public class PersonaTest {

    Persona persona;

    public PersonaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        persona = new Persona("Rodrigo", "Duarte", "Montevideo", "09/11/1992", "Uruguayo", "/Imagenes/addPerson3.png", 1, 1, 1);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getNombre method, of class Persona.
     */
    @Test
    public void testGetNombre() {
        assertEquals("Rodrigo", persona.getNombre());
    }

    /**
     * Test of getApellido method, of class Persona.
     */
    @Test
    public void testGetApellido() {
        assertEquals("Duarte", persona.getApellido());
    }

    /**
     * Test of getLugar method, of class Persona.
     */
    @Test
    public void testGetLugar() {
        assertEquals("Montevideo", persona.getLugar());
    }

    /**
     * Test of getFechaNacimiento method, of class Persona.
     */
    @Test
    public void testGetFechaNacimiento() {
        assertEquals("09/11/1992", persona.getFechaNacimiento());
    }

    /**
     * Test of getNacionalidad method, of class Persona.
     */
    @Test
    public void testGetNacionalidad() {
        assertEquals("Uruguayo", persona.getNacionalidad());
    }

    /**
     * Test of getSexo method, of class Persona.
     */
    @Test
    public void testGetSexo() {
        assertEquals(1, persona.getSexo());
    }

    /**
     * Test of getEstadoCivil method, of class Persona.
     */
    @Test
    public void testGetEstadoCivil() {
        assertEquals(1, persona.getEstadoCivil());
    }

    /**
     * Test of getEstadoSalud method, of class Persona.
     */
    @Test
    public void testGetEstadoSalud() {
        assertEquals(1, persona.getEstadoCivil());
    }

    /**
     * Test of getImagen method, of class Persona.
     */
    @Test
    public void testGetImagen() {
        assertEquals("/Imagenes/addPerson3.png", persona.getImagen());
    }

    /**
     * Test of setNombre method, of class Persona.
     */
    @Test
    public void testSetNombre() {
        persona.setNombre("Sebastian");
        assertEquals("Sebastian", persona.getNombre());
    }

    /**
     * Test of setApellido method, of class Persona.
     */
    @Test
    public void testSetApellido() {
        persona.setApellido("Colina");
        assertEquals("Colina", persona.getApellido());
    }

    /**
     * Test of setLugar method, of class Persona.
     */
    @Test
    public void testSetLugar() {
        persona.setLugar("Canelones");
        assertEquals("Canelones", persona.getLugar());
    }

    /**
     * Test of setFechaNacimiento method, of class Persona.
     */
    @Test
    public void testSetFechaNacimiento() {
        persona.setLugar("09/11/1992");
        assertEquals("09/11/1992", persona.getFechaNacimiento());
    }

    /**
     * Test of setNacionalidad method, of class Persona.
     */
    @Test
    public void testSetNacionalidad() {
        persona.setNacionalidad("Uruguayo");
        assertEquals("Uruguayo", persona.getNacionalidad());
    }

    /**
     * Test of setSexo method, of class Persona.
     */
    @Test
    public void testSetSexo() {
        persona.setSexo(1);
        assertEquals(1, persona.getSexo());
    }

    /**
     * Test of setEstadiCivil method, of class Persona.
     */
    @Test
    public void testSetEstadiCivil() {
        persona.setEstadiCivil(1);
        assertEquals(1, persona.getEstadoCivil());
    }

    /**
     * Test of setEstadoSalud method, of class Persona.
     */
    @Test
    public void testSetEstadoSalud() {
        persona.setEstadoSalud(1);
        assertEquals(1, persona.getEstadoSalud());
    }

    /**
     * Test of setImagen method, of class Persona.
     */
    @Test
    public void testSetImagen() {
        persona.setImagen("/Imagenes/addPerson3.png");
        assertEquals("/Imagenes/addPerson3.png", persona.getImagen());
    }

    /**
     * Test of setPersonaVacia method, of class Persona.
     */
    @Test
    public void testSetPersonaVacia() {
        persona.setPersonaVacia(persona);
        assertEquals("", persona.getNombre());
        assertEquals("", persona.getApellido());
        assertEquals("", persona.getLugar());
        assertEquals(0, persona.getEstadoCivil());
        assertEquals("", persona.getFechaNacimiento());
        assertEquals("/Imagenes/addPerson3.png", persona.getImagen());
        assertEquals("", persona.getNacionalidad());
        assertEquals(0, persona.getSexo());
        assertEquals(1, persona.getEstadoSalud());
    }

    /**
     * Test of toString method, of class Persona.
     */
    @Test
    public void testToString() {
        assertEquals("Rodrigo Duarte", persona.toString());
    }
}
