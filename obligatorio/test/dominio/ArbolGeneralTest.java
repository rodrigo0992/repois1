/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rodrigo
 */
public class ArbolGeneralTest {

    public ArbolGeneralTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCantidad method, of class ArbolGeneral.
     */
    @Test
    public void testGetCantidad() {
        ArbolGeneral arbol = new ArbolGeneral();
        Persona persona2 = new Persona();
        persona2.setNombre("Rodrigo");
        Elemento<Persona> elem = arbol.insertarRaiz(persona2);
        Persona persona3 = new Persona();
        persona3.setNombre("Lucas");
        arbol.insertarHijo(elem, persona3);
        Persona persona1 = new Persona();
        persona1.setNombre("Raiz");
        elem = arbol.insertarPadreRaiz(elem, persona1);
        Persona persona4 = new Persona();
        persona4.setNombre("Sebastian");
        arbol.insertarHijo(elem, persona4);

        assertEquals(4, arbol.getCantidad());

    }

    /**
     * Test of esRaiz method, of class ArbolGeneral.
     */
    @Test
    public void testSetRaiz() {
        ArbolGeneral arbol = new ArbolGeneral();
        Persona persona = new Persona();
        persona.setNombre("Rodrigo");
        NodoGeneral<Persona> nodo = new NodoGeneral<Persona>(persona, null, 1);
        arbol.setRaiz(nodo);

        assertEquals(nodo, arbol.getRaiz());

    }

    /**
     * Test of esRaiz method, of class ArbolGeneral.
     */
    @Test
    public void testGetRaiz() {
        ArbolGeneral arbol = new ArbolGeneral();
        Persona persona = new Persona();
        persona.setNombre("Rodrigo");
        Elemento<Persona> elem = arbol.insertarRaiz(persona);

        assertEquals(elem, arbol.getRaiz());
    }

    /**
     * Test of esRaiz method, of class ArbolGeneral.
     */
    @Test
    public void testEsRaiz() {
        ArbolGeneral arbol = new ArbolGeneral();
        Elemento<Persona> elem = arbol.insertarRaiz(arbol);

        assertTrue(arbol.esRaiz(elem));
    }

    /**
     * Test of estaVacia method, of class ArbolGeneral.
     */
    @Test
    public void testEstaVacia() {
        ArbolGeneral arbol = new ArbolGeneral();
        assertEquals(0, arbol.getCantidad());
    }

    /**
     * Test of getElementoPorNodoId method, of class ArbolGeneral.
     */
    @Test
    public void testGetElementoPorNodoId() {
        ArbolGeneral arbol = new ArbolGeneral();
        Persona persona = new Persona();
        persona.setNombre("Rodrigo");
        Elemento<Persona> elem = arbol.insertarRaiz(persona);
        persona = new Persona();
        persona.setNombre("Padre");
        arbol.insertarPadreRaiz(elem, persona);

        NodoGeneral<Elemento<Persona>> nodo = new NodoGeneral<Elemento<Persona>>();
        arbol.getElementoPorNodoId(1, arbol.getRaiz(), nodo);

        assertEquals(1, arbol.getNodoId(nodo.getElemento()));

    }

    /**
     * Test of obtenerHijos method, of class ArbolGeneral.
     */
    @Test
    public void testObtenerHijos() {
        ArrayList<Persona> personas = new ArrayList<Persona>();
        ArbolGeneral arbol = new ArbolGeneral();
        Persona persona = new Persona();
        persona.setNombre("Rodrigo");
        personas.add(persona);
        Elemento<Persona> elem = arbol.insertarRaiz(persona);
        persona = new Persona();
        persona.setNombre("Padre");
        elem = arbol.insertarPadreRaiz(elem, persona);
        persona = new Persona();
        persona.setNombre("Sebastian");
        personas.add(persona);
        arbol.insertarHijo(elem, persona);

        ArrayList<Elemento<Persona>> hijos = arbol.obtenerHijos(elem);

        assertEquals(personas.get(0), hijos.get(0).getElemento());
        assertEquals(personas.get(1), hijos.get(1).getElemento());

    }

    /**
     * Test of insertarRaiz method, of class ArbolGeneral.
     */
    @Test
    public void testInsertarRaiz() {
        ArbolGeneral arbol = new ArbolGeneral();
        Persona persona = new Persona();
        persona.setNombre("Rodrigo");
        Elemento<Persona> elem = arbol.insertarRaiz(persona);

        assertEquals(persona, elem.getElemento());
    }

    /**
     * Test of insertarHijo method, of class ArbolGeneral.
     */
    @Test
    public void testInsertarHijo() {
        ArbolGeneral arbol = new ArbolGeneral();
        Persona persona = new Persona();
        persona.setNombre("Padre");
        Elemento<Persona> elemRaiz = arbol.insertarRaiz(persona);
        persona = new Persona();
        persona.setNombre("Rodrigo");
        Elemento<Persona> elemHijo = arbol.insertarHijo(elemRaiz, persona);

        assertEquals(persona, elemHijo.getElemento());

    }

    /**
     * Test of insertarPadreRaiz method, of class ArbolGeneral.
     */
    @Test
    public void testInsertarPadreRaiz() {
        ArbolGeneral arbol = new ArbolGeneral();
        Persona persona = new Persona();
        persona.setNombre("Raiz");
        Elemento<Persona> elem = arbol.insertarRaiz(persona);
        persona = new Persona();
        persona.setNombre("Padre");
        elem = arbol.insertarPadreRaiz(elem, persona);

        assertEquals(persona, elem.getElemento());
    }

    /**
     * Test of setPareja method, of class ArbolGeneral.
     */
    @Test
    public void testSetPareja() {
        ArbolGeneral arbol = new ArbolGeneral();
        Persona persona = new Persona();
        persona.setNombre("Raiz");
        Elemento<Persona> elem = arbol.insertarRaiz(persona);
        persona = new Persona();
        persona.setNombre("Pareja");
        Persona pareja = (Persona) arbol.setPareja(elem, persona);

        assertEquals(persona, pareja);
    }

    /**
     * Test of tienePareja method, of class ArbolGeneral.
     */
    @Test
    public void testTienePareja() {
        ArbolGeneral arbol = new ArbolGeneral();
        Persona persona = new Persona();
        persona.setNombre("Persona");
        Elemento<Persona> elem = arbol.insertarRaiz(persona);
        Persona pareja = new Persona();
        persona.setNombre("Pareja");
        arbol.setPareja(elem, pareja);

        assertTrue(arbol.tienePareja(elem));
    }

    /**
     * Test of borrarPersona method, of class ArbolGeneral.
     */
    @Test
    public void testBorrarPersona() {
        Persona persona = new Persona("Rodrigo", "Duarte", "Montevideo", "09/11/1992", "Uruguayo", "/Imagenes/addPerson3.png", 1, 1, 1);
        ArbolGeneral arbol = new ArbolGeneral();
        arbol.borrarPersona(persona);

        assertEquals("", persona.getNombre());
        assertEquals("", persona.getApellido());
        assertEquals("", persona.getLugar());
        assertEquals(0, persona.getEstadoCivil());
        assertEquals("", persona.getFechaNacimiento());
        assertEquals("/Imagenes/addPerson3.png", persona.getImagen());
        assertEquals("", persona.getNacionalidad());
        assertEquals(0, persona.getSexo());
        assertEquals(1, persona.getEstadoSalud());
    }

    /**
     * Test of recorridoPreOrden method, of class ArbolGeneral.
     */
    @Test
    public void testRecorridoPreOrden() {
        ArbolGeneral arbol = new ArbolGeneral();
        Persona persona2 = new Persona();
        persona2.setNombre("Rodrigo");
        Elemento<Persona> elem = arbol.insertarRaiz(persona2);
        Persona persona3 = new Persona();
        persona3.setNombre("Lucas");
        arbol.insertarHijo(elem, persona3);
        Persona persona1 = new Persona();
        persona1.setNombre("Raiz");
        elem = arbol.insertarPadreRaiz(elem, persona1);
        Persona persona4 = new Persona();
        persona4.setNombre("Sebastian");
        arbol.insertarHijo(elem, persona4);

        ArrayList<Elemento<Persona>> lista = arbol.recorridoPreOrden();

        assertEquals(persona1, lista.get(0).getElemento());
        assertEquals(persona2, lista.get(1).getElemento());
        assertEquals(persona3, lista.get(2).getElemento());
        assertEquals(persona4, lista.get(3).getElemento());

    }

    /**
     * Test of recorridoPreOrden method, of class ArbolGeneral.
     */
    @Test
    public void testObtenerPareja() {
        ArbolGeneral<Persona> arbol = new ArbolGeneral<Persona>();
        Persona persona = new Persona();
        persona.setNombre("Rodrigo");
        Persona pareja = new Persona();
        pareja.setNombre("Pareja");
        NodoGeneral<Persona> nodo = new NodoGeneral<Persona>(persona, null, 1);
        nodo.setPareja(pareja);
        arbol.setRaiz(nodo);

        assertEquals(pareja, arbol.obtenerPareja(nodo));

    }
}
